# Network inference methods using two types of data: gene expression data and snp data for hybrid genotypes

Authors : Lise Pomiès, Céline Brouard, Harold Duruflé, Élise Maigné, Clément Carré, Louise Gody, Fulya Trösser, George Katsirelos, Brigitte Mangin, Nicolas B Langlade, and Simon de Givry.

This repository contains scripts to infer a network using both gene expression and snp data.
This work has been used to produce the results of the article "Gene regulatory network inference methodology for genomic and transcriptomic data acquired in genetically related heterozygote individuals", Bioinformatics, 2022, https://doi.org/10.1093/bioinformatics/btac445. See also its [supplementary materials](supplementary.pdf).

A set of data files are provided to run the analysis. 
The complete 100 simulated datasets and the corresponding meta-analysis output networks are available [here](https://doi.org/10.15454/vrgwz2).

Several methods are used : 

 * Lasso (R)
 * OLS (R)
 * Random forest (R)
 * Bayesian network (C++/matlab)
 * PE-MRF (python)
 * findr (R)
 
Then a meta analysis is performed to combine the results. 

![workflow](workflow.png)

## Datasets

Some examples data are provided in the data folder to run the analysis :

 * reference network : exampleData_network.csv
 * gene expression : exampleData_expression.tsv
 * SNP : exampleData_haplotype.tsv

#### artificialDataSet_network.csv :

It's the reference network from which are simulated the expression data. 

The file contains two columns, separated by tabs ("\t") :

 * First column : regulator genes
 * Second column : regulated genes

The network consists in 143 genes (named G1 to G143) and 313 regulations (edges).

The network is a directed graph: there is a direction for each edge (source, target). 

#### exampleData_expression.tsv :

Contains the expression simulated for the 143 genes, in 463 genotypes (samples). 

Genes are in row (E1 to E143), genotypes in column. 

Columns are tab separated ("\t").
        
#### exampleData_haplotype.tsv :

Contains the haplotypes of the SNP associated to each gene.

Genes are in row (E1 to E143), genotypes (463) in column.

The SNP are coded :
 * 0 -> SNP wild homozygous
 * 1 -> SNP heterozygous
 * 2 -> SNP mutated homozygous

Columns are tab separated ("\t").






