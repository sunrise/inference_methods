##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# inférence de reseau avec asreml (modèle Brigitte)
# 2 modèles différents un pour exp -> exp et un pour snp -> exp
# utilise la kinship

# Prend en entrée plusieurs fichier :
# 1 - la matrice de kinship entre les génotypes
# 2 - expression des gènes d'interet dans les différents génotype
#        colonne : genotype
#        ligne : id_transcript (designé par id_exp dans le script)
# 3 - snp (associé aux gènes) pour les différents génotypes
#        colonne : genotype
#        ligne : id_snp
# le nom des 3 fichiers est précisé en argument du script R

# renvoie deux listes d'arrètes :
#     exp -> exp
#     snp -> exp

##########################################################################
#### LIBRARY #############################################################
##########################################################################

library(asreml)

#.libPaths("~/work/Rlib/")
setwd(dir = "~/sunrise")
repOut <- "RESULTS/100simulatedDataSet/" # On server


##########################################################################
#### FUNCTION ############################################################
##########################################################################

#' removeExt
#'
#' Remove any common extension from a vector of file names
#' by Gordon Smyth, 19 July 2002.  Last modified 19 Jan 2005.
#' 
#' @param x file name with extention and path
#'
#' @return file name without extension and paths
removeExt <- function(x) {
  x <- as.character(x)
  x <- basename(x)
  n <- length(x)
  ext <- sub("(.*)\\.(.*)$","\\2",x)
  if(all(ext[1] == ext))
    return(sub("(.*)\\.(.*)$","\\1",x))
  else
    return(x)
}

# ensemble de fonction pr inf avec asreml
source("scripts/OLS/mlmm_allmodels_withASREML.r")

#------------------------------------------------------------------------#

##########################################################################
#### PARAMETER ###########################################################
##########################################################################

# ARGUMENTS -------------------------------------------------------------#

# Recuperation des arguments
args <- commandArgs(trailingOnly=TRUE)
  # args 1 : gene expression data (columns = genotypes)
  # args 2 : snp data file (columns = genotypes)
  # args 3 : kinship matrix, dependence between genotypes
if(length(args)==0){
  args <- c("data/exampleData_expression.tsv",
            "data/exampleData_haplotype.tsv",
            "data/input_OLS/matrixKinshipNonNorm.forGWAS_AD.Rdata")
  args <- c("data/100simulatedDataSet/artificialDataSet_expression/G0_Z0/G0Z0_E1_expression.tsv",
            "data/100simulatedDataSet/artificialDataSet_haplotype.tsv",
            "data/matrixKinshipNonNorm.forGWAS_AD.Rdata") 
}

# PARAMETERS  --------------------------------------------------#

# generic name for output files/directories
outFileName <- paste(sapply(args, removeExt), collapse="")
outFileName <- removeExt(args[1])

# format des données TRUE = genotypes in rows, FALSE = genotypes in columns in source files
genotypesAsRows <- FALSE

# type de model utilisé
mod <-"withKinship"

# Location of output and temporary files
fileRep <- paste0("RESULTS/100simulatedDataSet/", outFileName, "/out_OLS")

##########################################################################
#### MAIN ################################################################
##########################################################################

# READ DATA -------------------------------------------------------------#

exp <- read.table(args[1], header = TRUE, sep = "\t")
snp <- read.table(args[2], header = TRUE, sep = "\t")
load(args[3])

# We want the genotypes in rows, if they are not we transpose the data
if(genotypesAsRows == FALSE) {
  exp <- as.data.frame(t(exp))
  snp <- as.data.frame(t(snp))
} else {
  exp <- as.data.frame(exp)
  snp <- as.data.frame(snp)
}

#------------------------------------------------------------------------#

parameters <- list("idGenotype" = sort(rownames(exp)),
             "idExp" = colnames(exp),
             "idSnp" = colnames(snp),
             "nGenotype" = nrow(exp),
             "nExp" = ncol(exp),
             "nSnp" = ncol(snp))

names(parameters$idExp) <- parameters$idExp
names(parameters$idSnp) <- parameters$idSnp

#------------------------------------------------------------------------#

# Same order for the files and matrix, and corresponding rows (and columns for matrix)

exp <- exp[parameters$idGenotype,]
snp <- snp[parameters$idGenotype,]
# center and reduction of expression by column
exp <- scale(exp)

# get kinship of observed hybrid
Ka.hyb <- Ka[parameters$idGenotype, parameters$idGenotype]

# create kinship on data's genotype
kin.onGeno <- as.matrix(snp) %*% t(as.matrix(snp))

# begin analyses
# normalize and inverse kinships
kin.onGeno.inv <- inv.matrix.sdp(matrix = normalize.matrix(kin.onGeno))
ka.hyb.inv <- inv.matrix.sdp(matrix = normalize.matrix(Ka.hyb))

# Get names
hyb.snp <- as.factor(colnames(kin.onGeno))
hyb.add <- as.factor(colnames(Ka.hyb))

# redirige temporairement la sortie
#sink(paste0(file.path(outDirGlobal, outDir),"/network.withkinship.log"))

# MODEL -----------------------------------------------------------------#

# model for exp -> exp (asreml)
print("Exp -> Exp")
pval.exp <- as.data.frame(lapply(X = parameters$idExp, function(idGene) {

  iG <- which(colnames(exp) == idGene)
  
  # formule du model :
  fixed.mod <- my.model.formula(name.trait = colnames(exp)[iG], name.exp = colnames(exp)[-iG])
  
  print(fixed.mod)
  # modele asreml
  res.asreml <- asreml(fixed = fixed.mod,
                       random = ~ giv(hyb.snp, var = TRUE) + giv(hyb.add, var = TRUE),
                       data = data.frame(exp, hyb.snp, hyb.add),
                       ginverse = list(hyb.snp = kin.onGeno.inv, hyb.add = ka.hyb.inv),
                       Cfixed = TRUE,
                       na.method.X = "omit",
                       na.method.Y = "omit",
                       maxiter = 20)

  # blue and variance-covariance of blue
  blue <- res.asreml$coefficients$fixed
  varcov.blue <- summary(res.asreml)$Cfixed
  wald.test <- blue ^ 2 / diag(varcov.blue)
  pval.wald <- pchisq(wald.test, 1, lower.tail = FALSE)
  pval.wald[idGene] <- NA

  return(pval.wald[parameters$idExp])
}))

# model for snp -> exp (asreml)
print("Snp -> Exp")
pval.snp <- as.data.frame(lapply(X = parameters$idExp, function(idGene) {

  iG = which(colnames(exp) == idGene)
  
  # recupère l'expression du gène
  phenotype <- exp[,iG]
  result.mlmm <- mlmm_allmodels(Y = phenotype,
                                XX = list(snp),
                                KK = list(Ka.hyb),
                                nbchunks = 2,
                                maxsteps = 2,
                                cofs = exp[,-iG],
                                female = NULL,
                                male = NULL)

  pval<-result.mlmm[[2]]

  return(pval[parameters$idSnp])
}))

##########################################################################
#### SAVE ################################################################
##########################################################################
# creating the repertory if needed
fileRepTemp <- paste0(fileRep, "/temp/")
if(!dir.exists(fileRepTemp)){
  dir.create(fileRepTemp, recursive = T)
}

write.csv(pval.exp, file = paste0(fileRepTemp,"/pval_exp_",mod,".csv"))
write.csv(pval.snp, file = paste0(fileRepTemp,"/pval_snp_",mod,".csv"))

# Remove the R connections
#sink()
