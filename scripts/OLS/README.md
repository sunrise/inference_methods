# Network inference with OLS method using two types of data: gene expression data and snp data for different genotypes.

Two different models are used for edges between snp and gene expression and edges between expression and expression.

## Main script : [OLSnetworkWithKinship.r](https://forgemia.inra.fr/elise.maigne/inference_sunrise/-/blob/master/scripts/OLS/OLSnetworkWithKinship.r)

Parameters to set :

 * **args** : link to three data files (kinship matrix, expression and haplotypes)
     * arg 1 : kinship matrix between genotypes
     * arg 2 : gene expression data (rows : id_transcript - id_exp TODO, columns : genotype)
     * arg 3 : snp data for different genotypes (rows : id_snp, columns : genotypes)
 * **outFileName** : generic name for output files/directories
 * **mod** : model to use (*withKinship*)
 * **genotypesAsRows** : are the genotypes in rows in expression and snp data ? TRUE/FALSE

        
OLSnetworkWithKinship.R calls [mlmm_allmodels_withASREML.r](./scripts/OLS/mlmm_allmodels_withASREML.r) containing the functions. 


To launch directly the method on a terminal you can use the command 

    R CMD BATCH -q "--args filenameKinship filenameExpr filenameSnp " ./scripts/OLS/runOLSnetworkWithKinship.r

from the inferenceMethods directory, with *filenameKinship* the link to the Kinship matrix, 
*filenameExpr* the link to gene expression data file 
and *filenameSnp* is the link to SNP data file


Example :

    R CMD BATCH -q "--args data/inputOLS/matrixKinshipNonNorm.forGWAS_AD.Rdata 
    data/exampleData_expression.tsv data/exampleData_haplotype.tsv " 
    runOLSnetworkWithKinship.sh

To run the method on different datasets, this script can be parallelized using script **parallel.sh** (and indicating the right snp file in **runOLSnetworkWithKinship.sh**).
Example

    ./scripts/parallel.sh -j 8 ./scripts/OLS/runOLSnetworkWithKinship.sh data/100simulatedDataSet/*_expression.tsv

## Required package

`asreml`

`asreml` is a commercial package for mixed models using Residual Maximum Likelihood . 
Go to [https://www.vsni.co.uk/software/asreml/].

## Produced files are in

    * data/*outFileName*/out_OLS/
    * ........................../temp/pval_exp_*mod*.csv
    * ........................../temp/pval_snp_*mod*.csv






