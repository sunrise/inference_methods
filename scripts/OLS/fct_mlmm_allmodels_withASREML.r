###adapted from MLMM - Multi-Locus Mixed Model

##############################################################################################################################################
# SET OF FUNCTIONS TO CARRY GWAS CORRECTING FOR POPULATION STRUCTURE WHILE INCLUDING
# COFACTORS THROUGH A FORWARD REGRESSION APPROACH
# possible models : additive, additive+dominance, female+male, female+male+interaction
#
# #note: require ASREML
#
# #REQUIRED DATA & FORMAT
#
# PHENOTYPE - Y: A vector of length n, with names(Y)=individual names
# GENOTYPE  XX: A list of length one, two or three matrices depending on the models
#               Matrices are n by m matrix, where n=number of individuals, m=number of SNPs,
#	              with rownames(X)=individual names, and colnames(X)=SNP names
#               * additive: a single matrix, additive+dominance: two matrices
#               * female+male: two matrices with the female one first,
#               * female+male+interaction: three matrices with the female first, the male then the interaction
# KINSHIP  KK: a list of one, two or three matrices depending on the models
#               * additive: a n by n matrix, where n=number of individuals, with rownames()=colnames()=individual names
#               * additive+dominance: two n by n matrices, where n=number of individuals, with rownames()=colnames()=individual names
#               * female+male: a n.female by n.female matrix, with rownames()=colnames()=female names
#                              a n.male by n.male matrix, with rownames()=colnames()=male names
#               * female+male+interaction: the same two matrices as the model female+male
#                                          and a n by n matrix, where n=number of individuals, with rownames()=colnames()=individual names
# Factor - female: a factor of levels female names and length n, only for the last two models
# Factor - male: a factor of levels male names and length n, only for the last two models
# cofs:  a n by q matrix, where n=number of individuals, q=number of fixed effect,
#        with rownames()=individual names and with column names, 
#        forbidden head of column names for this matrix "eff1_" and usage of special characters as "*","/","&"
#
# Each of the previous data must be sorted in the same way, according to the individual name
#
# #FUNCTIONS USE
# save this file somewhere on your computer and source it!
#
# mlmm_allmodels(Y,XX,KK,nbchunks,maxsteps,cofs=NULL,female=NULL,male=NULL)
#
# nbchunks: an integer defining the number of chunks of matrices to run the analysis, 
#           allows to decrease the memory usage ==> minimum=2, increase it if you do not have enough memory
# maxsteps: maximum number of steps desired in the forward approach. 
#           The forward approach breaks automatically once the pseudo-heritability is close to 0,
#           however to avoid doing too many steps in case the pseudo-heritability does not reach 
#           a value close to 0, this parameter is also used.
#           It's value must be specified as an integer >= 3

#library(asreml)
require(asreml)

#------------------------------------------------------------------------#
#' Prep.forRSS
#'
#' computes matrices to accelerate the test computations
#'
#' @param istep
#' @param InvV
#' @param Y
#' @param cof_fwd
#' @param n
#'
#' @return
#' @export
Prep.forRSS <- function(istep, InvV, Y, cof_fwd, n) {
  Y_t <- crossprod(InvV , Y)
  cof_fwd_t <- crossprod(InvV, cof_fwd[[(istep - 1)]])
  Res_H0 <- summary(lm(Y_t ~ 0 + cof_fwd_t))$residuals
  Q_ <- qr.Q(qr(cof_fwd_t))
  M <- InvV %*% (diag(n) - tcrossprod(Q_, Q_))
  res <- list(M = M,
              cof_fwd_t = cof_fwd_t,
              Res_H0 = Res_H0)
  return(res)
}
#------------------------------------------------------------------------#
#' find.names
#'
#' @param xx
#'
#' @return
#' @export
#'
#' @examples
find.names <- function(xx) {
  res <- unique(unlist(strsplit (xx, "eff1_")))
  return(res)
}
#------------------------------------------------------------------------#

#' RSS.forSNP
#'
#' computes the residual sum of squares
#'
#' @param istep
#' @param list.Prep
#' @param nbchunks
#' @param XX
#'
#' @return
#' @export
#'
#' @examples
RSS.forSNP <- function(istep, list.Prep, nbchunks, XX) {
  #this function computes the residual sum of squares
  RSS <- list()
  XXt <- list()
  names.cof_fwd <- find.names(colnames(list.Prep$cof_fwd_t))
  coltokeep <- which(!colnames(XX[[1]]) %in% names.cof_fwd)
  if (round(length(coltokeep) / nbchunks) != 0) {
    for (j in 1:(nbchunks - 1)) {
      coltoget <-
        ((j - 1) * round(length(coltokeep) / nbchunks) + 1):(j * round(length(coltokeep) /
                                                                         nbchunks))
      for (ki in 1:length(XX)) {
        XXt[[ki]] <-
          crossprod(list.Prep$M , as.matrix(XX[[ki]][, coltokeep])[, coltoget])
      }
      RSS[[j]] <- unlist(sapply(1:ncol(XXt[[1]]), function(iij) {
        x <- NULL
        for (ki in 1:length(XX)) {
          x <- cbind(x, XXt[[ki]][, iij])
        }
        temp <- lsfit(x, list.Prep$Res_H0, intercept = FALSE)
        res <- c(sum(temp$residuals ^ 2), temp$qr$rank)
        res
      }))
    }
  }
  if (((nbchunks - 1) * round(length(coltokeep) / nbchunks) + 1) <= length(coltokeep)) {
    coltoget <-
      (((nbchunks - 1) * round(length(coltokeep) / nbchunks) + 1):length(coltokeep))
    for (ki in 1:length(XX)) {
      XXt[[ki]] <-
        crossprod(list.Prep$M , as.matrix(XX[[ki]][, coltokeep])[, coltoget])
    }
    RSS[[nbchunks]] <-
      unlist(sapply(1:ncol(XXt[[1]]), function(iij) {
        x <- NULL
        for (ki in 1:length(XX)) {
          x <- cbind(x, XXt[[ki]][, iij])
        }
        temp <- lsfit(x, list.Prep$Res_H0, intercept = FALSE)
        res <- c(sum(temp$residuals ^ 2), temp$qr$rank)
        res
      }))
  }
  rm(XXt)
  RES <- matrix(unlist(RSS), ncol = 2, byrow = T)
  rownames(RES) <- colnames(XX[[1]])[coltokeep]
  colnames(RES) <- c("RSS", "rank")
  return(RES)
}

#------------------------------------------------------------------------#
#' my.pval.aov
#'
#' compute p value of fixed effect
#'
#' @param addcof_fwd
#' @param cof_fix
#' @param InvV
#' @param Y
#' @param XX
#'
#' @return
my.pval.aov <- function(addcof_fwd, cof_fix, InvV, Y, XX) {
  Y_t <- crossprod(InvV, Y)
  reg <- list()
  reg <- lapply(2:length(addcof_fwd), function(ii) {
    res <- NULL
    for (ki in 1:length(XX)) {
      res <-
        cbind(res, XX[[ki]][, colnames(XX[[1]]) %in% addcof_fwd[[ii]]])
    }
    res <- crossprod(InvV, res)
    res
  })
  fix <- crossprod(InvV, cof_fix)
  model <- "Y_t~0+fix"
  for (i in 1:(length(addcof_fwd) - 1)) {
    model <- paste0(model, "+reg[[", i, "]]")
  }
  model <- as.formula(model)
  temp <- drop1(aov(model), test = "F")  #type II Sum of squares
  rownames.wob <-
    unlist(sapply(rownames(temp), function(xn) {
      unlist(strsplit(xn, " "))
    }))
  if ("fix" %in% rownames.wob) {
    res <- temp$"Pr(>F)"[-c(1, 2)]
  } else {
    res <- temp$"Pr(>F)"[-1]
  }
  names(res) <-
    paste0("selec_", unlist(addcof_fwd)[2:length(addcof_fwd)])
  return(res)
}
#------------------------------------------------------------------------#
#' ind.covariances
#'
#' contribution to individual covariances
#'
#' @param K
#' @param fact
#'
#' @return
ind.covariances <- function(K, fact) {
  RES <- matrix(unlist(sapply(fact, function(ii) {
    unlist(sapply(fact , function(jj) {
      il <- which(rownames(K) == levels(fact)[ii])
      ic <- which(colnames(K) == levels(fact)[jj])
      res <- K[il, ic]
    }))
  })), ncol = length(fact))
  return(RES)
}
#------------------------------------------------------------------------#
#' inv.matrix.sdp
#'
#' inversion of semi definite matrix
#'
#' @param matrix matrix to inverse
#' @param cst default = 0.00000001
#'
#' @return res: inverse of the matrix
inv.matrix.sdp <- function(matrix, cst = 0.00000001) {
  # projection of the matrix on definite positive matrix cone and inversion
  mat.decomp <- eigen(matrix, symmetric = TRUE)
  valpp.mat <- mat.decomp$values
  valpp.mat[which(valpp.mat < cst)] <-
    cst  # transform too small or negative value
  valpp.mat.inv <- 1 / valpp.mat
  res <-
    mat.decomp$vectors %*% (diag(valpp.mat.inv)) %*% t(mat.decomp$vectors)
  colnames(res) <- colnames(matrix)
  rownames(res) <- rownames(matrix)
  return(res)
}

#------------------------------------------------------------------------#
#' normalize.matrix
#'
#' Normalize a given matrix
#'
#' @param mat matrix to normalize
#'
#' @return mat.norm: normalized matrix
normalize.matrix <- function(mat) {
  n <- ncol(mat)
  mat.norm <- (n - 1) / sum((diag(n) - matrix(1, n, n) / n) * mat)  * mat
  return(mat.norm)
}

#' my.model.formula
#'
#'Write the formula that will be resolved by asreml
#'
#' @param name.trait name/id of the var to explain (expression of a gene)
#' @param name.exp names/id of the explanatory variables (expression of the others genes)
#' @param name.geno (default null)
#'
#' @return formula to resolve
my.model.formula <-
  function(name.trait, name.exp, name.geno = NULL) {
    if (is.null(name.geno)) {
      term <- paste0(" + ", name.exp)
    } else{
      term <- paste0(" + ", c(name.exp, name.geno))
    }
    term.model <- do.call(paste, c(as.list(term), sep = ""))
    model <- paste0(name.trait , "~ 1", term.model)
    model <- as.formula(model)
    
    return(model)
  }

#------------------------------------------------------------------------#
#' my.model
#'
#' Construit la formule du model que resoudra Asreml
#'
#' @param cof.names Identifiant des variables explicatives
#'
#' @return formule du modele pour Asreml
my.model <- function(cof.names) {
  res <- paste("+", cof.names, seq = '', collapes = '')
  res <- do.call(paste, c(as.list(res), sep = ""))
  res <- as.formula(paste0 ("Y~0", res))
  res
}

#------------------------------------------------------------------------#

#' Random effects of the asreml model
#'
#' @param eff.names 
#'
#' @return
my.random <- function(eff.names) {
  res <- paste0("+giv(", eff.names, ", var=TRUE)")
  res[1] <- paste0("~", unlist(strsplit(res[1], "[+]"))[2])
  res <- do.call(paste, c(as.list(res), sep = ""))
  res <- as.formula(res)
  res
}

#------------------------------------------------------------------------#
#' mlmm_allmodels
#'
#' n: number of genotype, q: number of fixed effect
#' Each of the previous data must be sorted in the same way, according to the individual name
#'
#' @param Y Phenotype: gene expression among the different genotype
#' @param XX Snp: list of 1 to 3 matrices (depending of the model). 
#'    Each matrix contain for each snp (in col) their type in each genotype(row). 4 possible models:
#'     * additive: 1 matrix,
#'     * additive+dominance: 2 matrix
#'     * female+male: 2 matrix with the female one first,
#'     * female+male+interaction: 3 matrices with the female first, the male then the interaction
#' @param KK a list of one, two or three kinship matrices depending on the models:
#'     * additive: a n by n matrix, where n=number of individuals, with rownames()=colnames()=individual names
#'     * additive+dominance: two n by n matrices, where n=number of individuals, with rownames()=colnames()=individual names
#'     * female+male: a n.female by n.female matrix, with rownames()=colnames()=female names
#'                    a n.male by n.male matrix, with rownames()=colnames()=male names
#'     * female+male+interaction: the same two matrices as the model female+male
#'                                and a n by n matrix, where n=number of individuals, with rownames()=colnames()=individual names
#' @param nbchunks an integer defining the number of chunks of matrices to run the analysis, 
#'     allows to decrease the memory usage ==> minimum=2, increase it if you do not have enough memory
#' @param maxsteps maximum number of steps desired in the forward approach. 
#'     The forward approach breaks automatically once the pseudo-heritability is close to 0, 
#'     however to avoid doing too many steps in case the pseudo-heritability does not reach a value close to 0, 
#'     this parameter is also used. It's value must be specified as an integer >= 3
#' @param cofs (gene expression except gene of interest) a n by q matrix, where n=number of individuals,
#'     q =number of fixed effect, with rownames()=individual names and with column names,
#'     forbidden head of column names for this matrix "eff1_" and usage of special characters as "*","/","&"
#' @param female a factor of levels female names and length n, only for the last two models
#' @param male a factor of levels male names and length n, only for the last two models
#'
#' @examples mlmm_allmodels(Y,XX,KK,nbchunks,maxsteps,cofs=NULL,female=NULL,male=NULL)
#'
mlmm_allmodels <-
  function(Y,
           XX,
           KK,
           nbchunks,
           maxsteps,
           cofs = NULL,
           female = NULL,
           male = NULL) {
    
    n <- length(as.vector(Y))           # number of genotype
    m <- ncol(XX[[1]])                  # nb of explanatory variables (snp)
    ind <- as.factor(names(Y))          # list of genotypes 
    nb.effet <- length(XX)              # nb of effects (nb of matrix of expl. var)
    nb.level.byeffect <- rep(n, nb.effet)  
    
    nom.effet <- c("eff1", "eff2", "eff3")
    effet <- list()
    effet <-
      lapply(X = 1:nb.effet, function(x)
        ind) # complete effet
    
    # if female+male
    if (!is.null(female) & !is.null(male)) {
      if (is.factor(female) == FALSE) {
        female <- as.factor(female)
      }
      if (is.factor(male) == FALSE) {
        male <- as.factor(male)
      }
      n.female <- length(levels(female))
      n.male <- length(levels(male))
      
      if (nb.effet == 2) {
        nb.level.byeffect <- c(n.female, n.male)
        effet <- list(female, male)
      } else if (nb.effet == 3) {
        nb.level.byeffect <- c(n.female, n.male, n)
        effet <- list(female, male, ind)
      }
    }
    
    ### CONTROL THE DIMENSIONS -------------------------------------------#

    # nb of genotypes in mat. snp = nb of genotype in Y (expression)
    trash <-
      lapply(X = XX, function(x)
        stopifnot(nrow(as.matrix(x)) == n))
    
    # if several matrix of snp, same dimensions in all of them
    if (nb.effet > 1) {
      trash <-
        lapply(X = c(2:nb.effet), function(x)
          stopifnot(ncol(XX[[x]]) == m))
    }
    
    # nb of genotypes in matrice of genes expression
    stopifnot(nrow(cofs) == n)
    
    # length of the matrix = nb of effects
    stopifnot(length(KK) == nb.effet)
    
    # test nb of genotype in kinship = nb genotype in effects
    trash <-
      lapply(X = 1:nb.effet, function(ki)
        stopifnot((nrow(KK[[ki]]) == nb.level.byeffect[ki]) & 
                   ncol(KK[[ki]]) == nb.level.byeffect[ki]))

    # test nb of genotypes in effects
    trash <-
      lapply(X = 1:nb.effet, function(ki)
        stopifnot(length(effet[[ki]]) == n))
    
    stopifnot(nbchunks >= 2)
    stopifnot(maxsteps >= 2)
    
    ### INTERCEPT  ------------------------------------------------------#
    X0 <- as.matrix(rep(1, n))
    colnames(X0) <- "mu"
    
    #kinship normalisation
    KK.norm <-
      lapply(X = 1:nb.effet, function(x)
        normalize.matrix(mat = KK[[x]]))
    
    # inversion matrice kinship
    KK.inv <-
      lapply(X = 1:nb.effet, function(x)
        inv.matrix.sdp(matrix = KK.norm[[x]]))
    
    # contribution to individual covariances
    KK.cov <-
      lapply(X = 1:nb.effet, function(x)
        ind.covariances(K = KK.norm[[x]], fact = effet[[x]]))
    
    #assignation to global environment for ASREML
    names(effet) <- nom.effet[1:nb.effet]
    names(KK.inv) <- nom.effet[1:nb.effet]
    assign("KK.inv", KK.inv, globalenv())
    
    # step 0 : NULL MODEL -----------------------------------------------#
    addcof_fwd <- list()
    addcof_fwd[[1]] <- 'NA'
    #
    cof_fwd <- list()
    if (is.null(cofs)) {
      cof_fwd[[1]] <- X0
    } else{
      cof_fwd[[1]] <- cbind(X0, as.matrix(cofs))
    }
    mod_fwd <- list()
    
    ### ASREML ----------------------------------------------------------#
    mod.formula <- my.model(colnames(cof_fwd[[1]]))
    mod.random <- my.random(names(effet))
    print(mod.formula)
    
    if (nb.effet == 1){
      mod_fwd[[1]] <- summary(asreml(fixed = mod.formula,
                                     random = mod.random,
                                     data = data.frame(effet, Y, cof_fwd[[1]]),
                                     ginverse = list(eff1 = KK.inv[[1]]),
                                     na.method.X = "omit",
                                     na.method.Y = "omit"))
    } else if (nb.effet == 2){
      mod_fwd[[1]] <- summary(asreml(fixed = mod.formula,
                                     random = mod.random,
                                     data = data.frame(effet, Y, cof_fwd[[1]]),
                                     ginverse = list(eff1 = KK.inv[[1]],
                                                     eff2 = KK.inv[[2]]),
                                     na.method.X = "omit",
                                     na.method.Y = "omit"))
    } else if (nb.effet == 3){
      mod_fwd[[1]] <- summary(asreml(fixed = mod.formula,
                                     random = mod.random,
                                     data = data.frame(effet, Y, cof_fwd[[1]]),
                                     ginverse = list(eff1 = KK.inv[[1]],
                                                     eff2 = KK.inv[[2]],
                                                     eff3 = KK.inv[[3]]),
                                     na.method.X = "omit",
                                     na.method.Y = "omit"))
    }
    
    
    df1 <- list()
    df1[[1]] <- n - mod_fwd[[1]]$nedf
    
    # Compute pseudoh
    pseudoh_fwd <- vector("list", maxsteps)
    pseudoh_fwd[[1]] <-
      sum(mod_fwd[[1]]$varcomp[-(nb.effet + 1), 2]) / sum(mod_fwd[[1]]$varcomp[, 2])
    # Elise
    # herit_fwd <- list()
    # herit_fwd[[1]] <-
    #   sum(mod_fwd[[1]]$varcomp[-(nb.effet + 1), 2]) / sum(mod_fwd[[1]]$varcomp[, 2])
    V <- mod_fwd[[1]]$varcomp[(nb.effet + 1) , 2] * diag(n)
    for (ki in 1:nb.effet) {
      V <- V + mod_fwd[[1]]$varcomp[ki, 2] * KK.cov[[ki]]
    }
    
    InvV <- solve(chol(V))

    # Initialization
    pval <- list()
    pval[[1]] <- NA
    fwd_lm <- list()
    pval_cof_fwd <- vector("list", maxsteps+1)
    # Elise
    # pval_cof_fwd <- list()
    # pval_cof_fwd[[1]] <- NULL
    # pval_cof_fwd[[2]] <- NULL
    cat('null model done! pseudo-h=', round(pseudoh_fwd[[1]], 3), '\n')
    
    # Other steps #####################################"
    for (i in 2:(maxsteps)) {
      if (pseudoh_fwd[[i - 1]] < 0.01 & i > 2) {
        break
      } else {
        list.Prep <- Prep.forRSS(
          istep = i,
          InvV = InvV,
          Y =  Y,
          cof_fwd =  cof_fwd,
          n =  n
        )
        RSS_H1.rank <- RSS.forSNP(
          istep = i,
          list.Prep = list.Prep,
          nbchunks = nbchunks,
          XX = XX
        )
        RSS_H1 <- RSS_H1.rank[, 1]
        RSS_H0 <- sum(list.Prep$Res_H0 ^ 2)
        df2 <- n - RSS_H1.rank[, 2] - df1[[i - 1]]
        df1.test <- RSS_H1.rank[, 2]
        Ftest <-
          (rep(RSS_H0, length(RSS_H1)) / RSS_H1 - 1) * df2 / df1.test
        Ftest[is.na(Ftest)] <- 0
        pval[[i]] <- pf(Ftest, df1.test, df2, lower.tail = FALSE)
        addcof_fwd[[i]] <- names(which(RSS_H1 == min(RSS_H1))[1])
        cof_fwd[[i]] <- cof_fwd[[(i - 1)]]
        for (ki in 1:nb.effet) {
          cof_fwd[[i]] <-
            cbind(cof_fwd[[i]], XX[[ki]][, colnames(XX[[1]]) %in% addcof_fwd[[i]]])
        }
        colnames(cof_fwd[[i]])[(ncol(cof_fwd[[i]]) - (nb.effet - 1)):ncol(cof_fwd[[i]])] <-
          c(paste0(nom.effet[1:nb.effet], "_", addcof_fwd[[i]]))
        #ASREML
        mod.formula <- my.model(cof.names = colnames(cof_fwd[[1]]))
        print(mod.formula)
        
        if (nb.effet == 1){
          mod_fwd[[i]] <- summary(asreml(fixed = mod.formula,
                                         random = mod.random,
                                         data = data.frame(effet, Y, cof_fwd[[i]]),
                                         ginverse = list(eff1 = KK.inv[[1]]),
                                         na.method.X = "omit",
                                         na.method.Y = "omit"
          )
          )
        } else if (nb.effet == 2){
          mod_fwd[[i]] <- summary(asreml(fixed = mod.formula,
                                         random = mod.random,
                                         data = data.frame(effet, Y, cof_fwd[[i]]),
                                         ginverse = list(eff1 =KK.inv[[1]],
                                                         eff2 = KK.inv[[2]]),
                                         na.method.X = "omit",
                                         na.method.Y = "omit"
          )
          )
          
        } else if (nb.effet == 3){
          mod_fwd[[i]] <- summary(asreml(fixed = mod.formula,
                                         random = mod.random,
                                         data = data.frame(effet, Y, cof_fwd[[i]]),
                                         ginverse = list(eff1 = KK.inv[[1]],
                                                         eff2 = KK.inv[[2]],
                                                         eff3 = KK.inv[[3]]),
                                         na.method.X = "omit",
                                         na.method.Y = "omit"
          )
          )
        }
        
        df1[[i]] <- n - mod_fwd[[i]]$nedf
        # Compute pseudo h
        pseudoh_fwd[[i]] <-
          sum(mod_fwd[[i]]$varcomp[-(nb.effet + 1), 2]) / sum(mod_fwd[[i]]$varcomp[, 2])
        rm(list.Prep)
        cat('step ', i - 1, ' done! pseudo-h=', round(pseudoh_fwd[[i]], 3), '\n')
      }
      
      ##get pval at each forward step for selected SNP
      V <- mod_fwd[[i]]$varcomp[(nb.effet + 1) , 2] * diag(n)
      
      for (ki in 1:nb.effet) {
        V <- V + mod_fwd[[i]]$varcomp[ki, 2] * KK.cov[[ki]]
      }
      
      InvV <- solve(chol(V))
      pval_cof_fwd[[i + 1]] <- my.pval.aov(
        addcof_fwd = addcof_fwd,
        cof_fix = cof_fwd[[1]],
        InvV = InvV,
        Y = Y,
        XX = XX
      )
      pval[[i]] <- c(pval_cof_fwd[[i]], pval[[i]])
    }
    
    #end of function
    return(pval)
  }


