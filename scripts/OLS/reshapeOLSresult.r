##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

## Traitement des matrice d'aretes obtenues avec OLS pour obtenir des
# listes d'arètes

##########################################################################
#### LIBRAIRY ############################################################
##########################################################################

library(reshape)

##########################################################################
#### FUNCTION ############################################################
##########################################################################

##########################################################################
#### PARAMETERS ##########################################################
##########################################################################

# Déclaration  ou  recuperation des arguments
args <- commandArgs(trailingOnly=TRUE)
#args <- c("100SimulatedDataset/G0_Z0/artificialDataset_G0Z0_expe1_expression/pval_exp_withKinship.csv")
dir <- getwd()
#dir <- "/home/lpomies/Documents/2019/1-InferenceMethodTest/1-methods/OLS/2-results"

# emplacement de la matrix de pValue de ttes les arètes possible

infileEdgeMatrix <- file.path(dir, args[1])
print(infileEdgeMatrix)

# emplacement du fichier de liste d'aretes
outfileEdgeList <- sub(pattern = "pval_",replacement = "edgeList_", x = infileEdgeMatrix, ignore.case = TRUE)
outfileEdgeList <- gsub("/temp/", "", outfileEdgeList)

print(outfileEdgeList)
##########################################################################
#### MAIN ################################################################
##########################################################################

#read data
pValData <- as.data.frame(read.csv(file = infileEdgeMatrix,
                                   row.names = 1,
                                   stringsAsFactors = FALSE))

pValData$Gj <- rownames(pValData)

# reshape data
pVal <- melt(data = pValData, id.vars = "Gj", variable_name = "Gi")

# ordonne les arcs (de la plus forte pValue à la plus faible)
pValOrder = pVal[order(pVal$value,decreasing = FALSE),c("Gj","Gi","value")]

edgeList <- pValOrder[which(!is.na(pValOrder$value)),]


##########################################################################
#### SAVE ################################################################
##########################################################################

# Enregistrement des listes d'arètes
write.table(x = edgeList, file = outfileEdgeList, sep = "\t", row.names = FALSE, col.names = TRUE, quote = FALSE)
