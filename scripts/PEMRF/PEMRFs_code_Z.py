#!/usr/bin/python
import numpy as np
from numpy import linalg as la

def Amatrix(data, m_vec, dim_vec, balance, c):
    # data: sufficient statistic data
    # m_vec: array containing the number of nodes for each type of distribution (plus initial value 1)
    # dim_vec: array containing the dimension for each type of distribution (plus initial value 1)
    # balance = [1, balance1, balance2, balance3, balance4]
    # c: binary vector that indicates the nodes that are discrete random variables
    
    num_sam,d_num = np.shape(data)
        
    balance_vec = np.repeat(balance,m_vec*dim_vec)
    balance_mat = np.tile(balance_vec,(num_sam,1))
    
    data_2 = np.concatenate((np.ones([num_sam,1]),data),axis=1)
    new_data = np.divide(data_2,balance_mat)
        
    M = np.dot(new_data.T,new_data)/num_sam
    
    A = M + np.diag(c)
    
    return A

def Block_Extract(Z, row_ind, col_ind, List_indices):
    # extracts the submatrix of Z for the row row_ind and the colonne col_ind
    
    inter_row = List_indices[row_ind]
    inter_col = List_indices[col_ind]
    
    num_row = len(inter_row)
    num_col = len(inter_col)
    
    inter_row_2 = inter_row.reshape(num_row,1)
    inter_col_2 = inter_col.reshape(num_col,1)
    
    return Z[inter_row_2, inter_col_2.T]

def Zupdate(theta, U, Eta, m, dim, List_indices, norm_type):
    # Update each subblock of Z
        
    num_nodes = np.sum(m)-1
    
    temp_matrix = theta + U
    
    B = [ [] for i in range(num_nodes+1)]
   
    for i in range(num_nodes+1): # iterate from 0 to the number of nodes
        for j in range(num_nodes+1):
            Block_ij = Block_Extract(temp_matrix, i, j, List_indices)
            if i==0 or j==0 or i==j :
                B[i].append(Block_ij)
            else:
                if norm_type == 'group_lasso':
                    # Proximal of the l1/l2 norm
                    gamma = la.norm(Block_ij, 'fro')
                    B[i].append((1 - Eta[i,j]/gamma).clip(0) * Block_ij) # Block-wise soft-thresholding
                elif norm_type == 'sparse_group_lasso':
                    # Proximal of the l1 norm
                    B[i].append( (Block_ij - Eta[i,j]).clip(0) - (-Block_ij - Eta[i,j]).clip(0) ) # Soft-thresholding
                    
    # now B is [ [B0,0 , B0,1 , ... , B0,31], [B1,0 , B1,1 , ... , B1,31], ..., [B31,0, ..., B31,31]]
    Z_new = np.block(B)
    Z_new = (Z_new + Z_new.T)/2
        
    return Z_new

def createEmatrix(theta, m, dim, tolerance, List_indices):
    # Convert the learnt theta parameters to an adjacency matrix by taking the Frobenius norm of each subblock
    
    num_nodes = np.sum(m)-1
    
    E = np.zeros([num_nodes+1,num_nodes+1])
    for i in range(num_nodes+1):
        for j in range(num_nodes+1):
            temp = la.norm(Block_Extract(theta, i, j, List_indices), 'fro')
            if np.absolute(temp) >= tolerance:
                E[i,j] = np.absolute(temp)
            else:
                E[i,j] = 0

    return E[1:,1:]

def ADMM(A, K, lamb, W, num_sam, m, dim, edge_tol, epsilon_abs, epsilon_rel, rho_init, inflate, norm_type, Z_init, U_init):
    # ADMM (Alternating Direction Method of Multiplier) algorithm
    #
    # INPUTS:
    # A: matrix obtained using the function Amatrix
    # K: maximum number of iterations
    # lamb: regularization parameter
    # W: matrix containing the weights used in the regulatization term
    # num_sam: number of samples
    # m: array containing the number of nodes for each type of distribution (plus initial value 1)
    # dim: array containing the dimension for each type of distribution (plus initial value 1)
    # epsilon_abs/epsilon_rel: absolute/relative tolerances used in the termination criteria
    # rho_init: initial step size value in the ADMM algorithm
    # inflate: inflation/deflation rate used to update the ADMM step size
    # norm_type: 'group_lasso' (l1/l2 norm) or 'sparse_group_lasso' (l1 norm)
    # Z_init/U_init: initialization of Z and U
    
    # Initialization
    d = np.sum(np.multiply(m, dim))-1
    Z = np.copy(Z_init)
    U = np.copy(U_init)
    rho = np.copy(rho_init) # initial step size
    
    # Construct a list that gives the indices corresponding to each node
    Mvec = np.array(np.repeat(dim,m))[np.newaxis]
    List_indices = np.split(np.arange(d+1),np.cumsum(Mvec))
    del List_indices[-1]
    
    for k in range(K):
        
        eta1 = rho/num_sam # define eta for theta update
        eta2 = (lamb*W)/rho; # define eta2 for Z update
        
        prev_Z = np.copy(Z) # Book-keeping previous Z for computing the dual residual
        
        # Update Theta
        temp = eta1*(prev_Z-U) - A
        temp = (temp + temp.T)/2
        
        lambvec,Q = la.eigh(temp) #eigenvalue decomposition
        
        D_lamb = np.diag(lambvec + np.sqrt( np.power(lambvec,2) + 4*eta1 * np.ones(len(lambvec)) ) )
        Theta =  Q @ D_lamb @ (Q.T)
        Theta = (1/(2*eta1))* Theta
        Theta = (Theta + Theta.T)/2
        
        # Update Z
        Z = Zupdate(Theta, U, eta2, m, dim, List_indices, norm_type)
        
        # Update U
        U += Theta - Z
        
        # Check convergence
        R_primal = la.norm(Theta - Z, 'fro') # primal residual
        R_dual = rho * la.norm(Z - prev_Z, 'fro') # dual residual
        epsilon_dual = (d+1) * epsilon_abs + epsilon_rel * rho * la.norm(U, 'fro')
        epsilon_pri = (d+1) * epsilon_abs + epsilon_rel * max(la.norm(Theta, 'fro'), la.norm(Z, 'fro'))
        
        if R_primal <= epsilon_pri and R_dual <= epsilon_dual:
            print('primal and dual both met')
            break;
        
        # Update the step size
        if R_primal > 10*R_dual:
            rho *= inflate
            U /= inflate
        elif R_dual > 10*R_primal:
            rho /= inflate
            U *= inflate

    # END of Iteration
    print('Iter number :',k)

    # Convert the learnt parameters theta to obtain the predicted adjacency matrix
    E = createEmatrix(Z, m, dim, edge_tol, List_indices)
    E = E / np.sqrt(np.tensordot(np.diag(E),np.diag(E),axes=0)) # normalization
    E = (E + E.T)/2
    
    return Theta, Z, U, E



