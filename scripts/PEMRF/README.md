The file [network_inference.py](./scripts/PEMRF/network_inference.py) contains the script for inferring gene regulations from gene expressions and haplotypes.
This script uses the PE-MRF (Pairwise exponential Markov Random Field) approach proposed by Park et al. (AISTATS 2017).
It requires to have Python 3 installed.

Example of usage with the simulated dataset:

    python network_inference.py --haplotype_file "artificialDataSet_haplotype.tsv"      --expression_file "artificialDataSet_expression/G9_Z9/Sim_20180405_140527_Exp_1_gene_expression_matrix.tsv" --results_file_name "results_with_bootstrap/G9_Z9/res_Exp_10" --bootstrap_file "artificialDataSet_bootstraps.tsv" --bootstrap 21

The script can take several arguments as input:

    --haplotype_file: 	Name of the file containing the haplotypes of the SNPs associated with each gene
    --expression_file: 	Name of the file containing the gene expressions
    --bootstrap		Number of the considered bootstrap (for example between 1 and 50)
    --results_file_name: Name of the file where the intermediary results will be saved (without extension)
    --bootstrap_file	Name of the file containing the indices for the different bootstraps
    --norm_type: 		Norm type: "group_lasso" (l1/l2 norm) or "sparse_group_lasso" (l1 norm)
    --abs_tol: 		Absolute tolerance used in the termination criteria
    --rel_tol: 		Relative tolerance used in the termination criteria
    --edge_tol: 		The score of the edges below this value are set to 0
    --max_iter: 		Maximum number of iterations in the ADMM algorithm
    --admm_init: 		Initial step size value in the ADMM algorithm
    --admm_inflate: 	Inflation/deflation rate used to update the ADMM step size

Only the 4 first arguments are required: if no value is set for the other arguments, they are set to a default value

The bootstrap matrix can be generated by running the script [create_bootstrap_file.R](./scripts/create_bootstrap_file.R). 

The script can be called on each bootstraps by running [batch_network.sh](./scripts/PEMRF/running batch_network.sh). 

When the script has been applied to all the bootstraps, the script [combine_bootstrap_results.py](./scripts/PEMRF/combine_bootstrap_results.py) can be used to aggregate the results:

    python combine_bootstrap_results.py --dir 'G9_Z9' --exp 1

The script must be run 10 times, one for each directory.
