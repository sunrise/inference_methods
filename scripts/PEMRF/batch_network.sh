#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --array=0-49
module load python/3.7
python network_inference.py
    --haplotype_file "100simulatedDataSet/artificialDataSet_haplotype.tsv"
    --expression_file "100simulatedDataSet/artificialDataSet_expression/G9_Z9/Sim_20180405_140527_Exp_10_gene_expression_matrix.tsv"
    --bootstrap_file "100simulatedDataSet/artificialDataSet_bootstraps.tsv"
    --results_file_name "results_with_bootstrap/G9_Z9/res_Exp_10"
    --bootstrap ${SLURM_ARRAY_TASK_ID}
