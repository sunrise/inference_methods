#!/usr/bin/python
import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
import numpy as np
import pandas as pd
from scipy.stats import rankdata
import argparse

parser = argparse.ArgumentParser(description='Concatenate the bootstrap results.')
parser.add_argument('--dir', type=str, help='directory (for example G0_Z0).')
parser.add_argument('--exp', type=str, help='experiment(number between 1 and 10).')
parser.add_argument('--num_boot', type=int, default = 50, help='number of bootstraps.')
args = parser.parse_args()

exp = str(args.exp)
dir = args.dir

T = pd.read_csv('100simulatedDataSet/artificialDataSet_haplotype.tsv',sep="\t")
G = pd.read_csv('100simulatedDataSet/artificialDataSet_expression/G0_Z0/Sim_20180404_171529_Exp_1_gene_expression_matrix.tsv', sep="\t")

num_gene = 143

# sum over the 50 bootstrap
sum_tot = np.zeros((num_gene*2,num_gene*2))
for boot in range(args.num_boot):
    sum_boot = np.loadtxt('results_with_bootstrap/' + dir + '/res_Exp_' + exp + '_bootstrap=' + str(boot) +'.txt',delimiter=' ')
    sum_tot += sum_boot

# Remove the diagonal and convert the scores on the edges to a vector
iu = np.triu_indices(num_gene,1)
il = np.tril_indices(num_gene,-1)
iul0 = np.concatenate((iu[0],il[0]))
iul1 = np.concatenate((iu[1],il[1]))
iul = [iul0,iul1]

Edge_expr = sum_tot[:num_gene,:num_gene] # extract edges of type expr -> expr
Edge_marker = sum_tot[num_gene:,:num_gene] # extract edges of type SNP -> expr

Count_expr = np.concatenate((Edge_expr[iu],Edge_expr[il]))
Count_marker = np.concatenate((Edge_marker[iu],Edge_marker[il]))

# Compute the ranking
rank_expr = rankdata(Count_expr,method='ordinal')
rank_marker = rankdata(Count_marker,method='ordinal')

# Save the results in the format with the 3 columns
E_col = np.tile(np.array(G.index),(num_gene,1))
E_row = E_col.T
M_col = np.tile(np.array(T.index),(num_gene,1))
M_row = M_col.T

EE_tot = list(zip( E_row[iul], E_col[iul], rank_expr ))
ME_tot = list(zip( M_row[iul], E_col[iul], rank_marker ))

EE_tot_sorted = sorted(EE_tot, key=lambda x: x[2], reverse=True)
ME_tot_sorted = sorted(ME_tot, key=lambda x: x[2], reverse=True)

np.savetxt('results_with_bootstrap/' + dir + '/rank_marker_pemrf_Exp_' + exp + '.tsv', np.array(ME_tot_sorted), delimiter='\t', fmt=['%s', '%s', '%s'])
np.savetxt('results_with_bootstrap/' + dir + '/rank_expression_pemrf_Exp_' + exp + '.tsv', np.array(EE_tot_sorted), delimiter='\t', fmt=['%s', '%s', '%s'])



