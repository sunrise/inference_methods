#!/usr/bin/python
# coding: utf-8
import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
import numpy as np
from numpy import linalg as la
import argparse
import pandas as pd
from PEMRFs_code_Z import *

parser = argparse.ArgumentParser(description='Network inference using Pairwise Exponential Markov Random Fields (PEMRF).')
parser.add_argument('--haplotype_file', type=str, help='Name of the file containing the haplotypes of the SNPs associated with each gene.')
parser.add_argument('--expression_file', type=str, help='Name of the file containing the gene expressions.')
parser.add_argument('--bootstrap', type=int, help='Bootstrap number (between 1 and 50).')
parser.add_argument('--results_file_name', type=str, help='Name of the file in which the results will be saved (without extension).')
parser.add_argument('--bootstrap_file', type=str, help='Name of the file containing the bootstrap indices.')
parser.add_argument('--norm_type', type=str, default='group_lasso', help='Norm type: group_lasso (l1/l2 norm) or sparse_group_lasso (l1 norm).')
parser.add_argument('--abs_tol', type=float, default=1e-3, help='Absolute tolerance used in the termination criteria.')
parser.add_argument('--rel_tol', type=float, default=1e-3, help='Relative tolerance used in the termination criteria.')
parser.add_argument('--edge_tol', type=float, default=1e-16, help='The score of the edges below this value are set to 0.')
parser.add_argument('--max_iter', type=int, default=1000, help='Maximum number of iterations in the ADMM algorithm.')
parser.add_argument('--admm_init', type=float, default=4., help='Initial step size value in the ADMM algorithm.')
parser.add_argument('--admm_inflate', type=float, default=2., help='Inflation/deflation rate used to update the ADMM step size.')

# Get the arguments from the command line
args = parser.parse_args()

# Load data
T = pd.read_csv(args.haplotype_file,sep="\t") # haplotypes
G = pd.read_csv(args.expression_file, sep="\t") # gene expressions

# Load file containing the bootstrap indices
boot = pd.read_csv(args.bootstrap_file,sep="\t",header=None,index_col=0)
bv = boot.values
Gb = np.array([G[s] for s in bv[args.bootstrap,:]])
Tb = np.array([T[s] for s in bv[args.bootstrap,:]])

# Encode the haplotypes using dummy variables : 0 corresponds to [1 0 0], 1 to [0 1 0] and 2 to [0 0 1]
num_sample,num_gene = Tb.shape
T_marker = np.zeros([num_sample,num_gene*3])
for i in range(num_sample):
    for j in range(num_gene):
        T_marker[i,j*3+Tb[i,j]] = 1

# center and scale the gene expression data
Gb_center = Gb - (np.tile(np.mean(Gb,axis=0),[num_sample,1]))
Gb_scale = Gb_center / (np.tile(np.std(Gb,axis=0),[num_sample,1]))
Gb_scale = np.nan_to_num(Gb_scale) # replace nan by 0

# Concatenate the gene expression and haplotype data to obtain the sufficient statistics
data = np.concatenate((Gb_scale,T_marker),axis=1)

# We consider a pairwise Markov Random Field with num_gene expression nodes that are Gaussian
# and num_gene marker nodes that follow a categorical distribution

m = np.array([1, num_gene, num_gene]) # number of nodes for each type of distribution (plus initial value 1)
dim = np.array([1, 1, 3]) # dimension for each type of distribution (plus initial value 1)

d = np.sum(np.multiply(m, dim))-1 # the dimension of the augmented edge parameter matrix

# vector that indicates the nodes that are discrete random variables
c = np.zeros(sum(m*dim))
c[m[1]*dim[1]+1:] = 1

Mvec = np.array(np.repeat(dim,m))[np.newaxis]
W = np.sqrt(np.transpose(Mvec)*Mvec) # weights used in the regularization term

balance = np.array([1,1,la.norm(T_marker)/la.norm(Gb)])

A = Amatrix(data, m, dim, balance, c)

# Grid of values among which the regularization parameter will be selected
lamb = np.logspace(-7,3,100)

# Run the ADMM algorithm for each value of lambda
Adj_matrix = []
Z_init = np.ones([d+1,d+1])*0.2
U_init = np.zeros([d+1, d+1])
for i in range(len(lamb)):
    print(i,flush=True)
    optimal_theta, optimal_Z, optimal_U, Ematrix = ADMM(A, args.max_iter, lamb[i], W, num_sample, m, dim, args.edge_tol, args.abs_tol, args.rel_tol, args.admm_init,args.admm_inflate,args.norm_type, Z_init, U_init)
    
    Adj_matrix.append(Ematrix) # Predicted adjacency matrix

    # warm start: the initial parameters are chosen as the optimal parameters obtained at the previous step
    Z_init = np.copy(optimal_Z)
    U_init = np.copy(optimal_U)

# For each edge, compute the number of times this edge is predicted among the 100 lambda
nb_edge_per_lambda = np.zeros(Ematrix.shape)
for l in range(len(lamb)):
    nb_edge_per_lambda += 1*(Adj_matrix[l] >0)

my_file = os.path.expanduser(args.results_file_name)
np.savetxt(my_file + '_bootstrap=' + str(args.bootstrap) + '.txt', nb_edge_per_lambda, delimiter=' ',fmt='%i')

