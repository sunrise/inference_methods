
Discretisation of continuous variables using adapted k-means or Gaussian mixture models depending on the observed distribution is unimodal or multimodal.

MATLAB code written by Jimmy Vandel, PhD (2012).

Usage in MATLAB: runDiscretisation( inputfile, outputfile, number_of_values)

or from a SHELL: runDiscretisation.sh inputfile number_of_values
(it will generate a file inputfile.res)

Reference:
Matthieu Vignes, Jimmy Vandel, David Allouche, Nidal Ramadan-Alban, Christine Cierco-Ayrolles, Thomas Schiex, Brigitte Mangin, and Simon de Givry
Gene regulatory network reconstruction using bayesian networks, the dantzig selector, the lasso and their meta-analysis
PLoS ONE, 6(12), 2011 
