function [T,edge,center] = discret_adaptative(data,varargin)
%% effectue une discretisation qui dépend de la forme de l'histogramme (mono ou multi modal)
%% version utilisant une detection de pic,mode 1 corespond a un classement simple en fonction des pics
%% mode 2 utilise uniquement le nombre de pic pour rechercher le nombre de mode et utilise em par la suite 
%% mode 3 utilise uniquement le nombre de pic pour rechercher le nombre de mode initial et utilise em par la suite en ajoutant potentiellement une modalité
%%INPUT : tableau de val ou chaque colonnes représente un gène et chaque ligne un sample
%%OUTPUT : T=tableau discrétisé
%	 edge=représente les limites des classes


nbModMax=4;
[nbE nbG]=size(data);
T=zeros(nbE,nbG);
args = varargin;
nbargs = length(args);
mode=3;
nbMod=1;
center=[];
fixe=0;

%width=6;
%high=50;
%voisin=4;
%sensitive=2;
width=4;
high=50;
voisin=2;
sensitive=2;

	if nbargs > 0 
		for i=1:2:nbargs
		      switch args{i},
			case 'voisin', voisin=args{i+1}; 
			case 'sensitive'; sensitive=args{i+1};
			case 'width'; width=args{i+1};
			case 'high'; high=args{i+1};
			case 'mode'; mode=args{i+1};
			case 'fixe'; fixe=args{i+1};
			case 'nbModMax'; nbModMax=args{i+1};
		      end
		end
	end

		[n,m]=hist(data,100);
		ok=0;
		maxi=[];
		fin=[];
		if variation(data)<=5					%%cas ou les données varient trop peu
			[val,M]=variation(data);
			ok=1;
			nbMod=val;					%%nombre de mode==le nombre de valeur différentes
			M=sort(M);
			if nbMod>1
				edge=[];
				for k=1:(length(M)-1)
					edge=[edge ((M(k)+M(k+1))/2)]; 	%%pointe les limites des bins entre chacune des valeurs
				end%for k
				edge=sort(edge);
			else 
				edge=[data(1)];
			end
		else							%%recherche des bornes pour la discrétisation
		
			while ~ok
				x=fastsmooth(n,width,3); 		%lissage de la courbe
				sig=sign(diff(x));

				%for j=voisin+1:(length(sig)-voisin)%recherche de maxima locaux
				%sig=[ ones(1,voisin)*sig(1) ,sig, ones(1,voisin)*sig(length(sig)) ];
				for j=voisin+1:(length(sig)-voisin)
					if abs(sum(sig(j-voisin:j-1))+sum(sig(j+1:j+voisin)))<sensitive 			%limite du nombre de voisin devant au moins etre du meme signe			
					if sum(sig(j-voisin:j-1))>0 								%evite les creux
					if abs(x(j)-x(j-voisin))>(maximum(x)/high) || abs(x(j)-x(j+voisin))>(maximum(x)/high)	%s'assure d'un variation assez grande
						maxi=[maxi j-voisin];								%ajoute un nouveau pic
					end
					end
					end
				end %for j

				%%regroupement des centroides proches 
				for k=1:length(maxi)
					if length(intersect(find(fin>maxi(k)-voisin),find(fin<maxi(k)+voisin)))==0
						fin=[fin maxi(k)];
					end
				end%for k

				if length(fin)>nbModMax && high>2				%si on a trouvé trop de pics
					fin=[];							%on remet tout à zero
					maxi=[];			
					high=high-1;						%on rehausse le seuil de detection et rebelote
				else
					if length(fin)==0 					%si on a pas trouvé de pic (pas besoin de remettre à zero)
						width=width+1;					%on augmente le lissage et rebelote
					else
						ok=1;						%on valide la detection de pic
						nbMod=ceil(length(fin));			%initialisation du nbMod avec le nb de pic (a voir)		
					end
				end


			end %while

		%figure;
		%plot(m,x)
		%hold on;
		%plot(m(fin),zeros(length(fin)),'xr')
		%hold off;
		%figure;

			width=7;
			if nbMod==1 									%un seul pic detecté
				%'call kmeans1'
				[T,edge,center] = discret_by_kmeans(data,5);					%on fait du kmean
			else									
				edge=[];									%plusieurs pics detectés on part sur les gaussiennes

				if mode==2									%recherche avec le nombre de pics détéctées
					if(fixe==0)
						[W,M,V,L] = EM_GM_fast(data,nbMod,[], [],0,[]);		%on initialise que le nombre de gaussiennes
					else	
						t=zeros(1,1,nbMod);					%on initialise aussi les points de départ
						for i=1:nbMod
							t(i)=0.001;						%%corespond aux covariances des gaussiennes
						end
						[W,M,V,L] = EM_GM_fast(data,nbMod,[],[],0,struct('W',repmat([1/nbMod],1,nbMod),'M',m(fin),'V',t));
					end
					M=sort(M(1,:));
				%%%%%%%%%%%%%%%%%%%%
				elseif mode==3									%recherche evolutive en partant du nombre de pic detectés
					Lbest=inf;
					[W,M,V,L] = EM_GM_fast(data,nbMod,[], [],0,[]);
					okb=0;
					while nbMod<nbModMax && ~okb && nbMod<variation(data)										
						nbMod=nbMod+1;							%%on fait une recherche avec un nombre de mode superieur pour voir si ça améliore 
						Lprev=L;
						if(fixe==0)
							[W,M,V,L] = EM_GM_fast(data,nbMod,[], [],0,[]);		
						else	
							t=zeros(1,1,nbMod);					
							for i=1:nbMod
								t(i)=0.001;					
							end
							[W,M,V,L] = EM_GM_fast(data,nbMod,[],[],0,struct('W',repmat([1/nbMod],1,nbMod),'M',m(fin),'V',t));
						end
						%okb=L*1/2*nbMod*log(length(data(:,i)))<Lprev*1/2*(nbMod-1)*log(length(data(:,i)));
						okb=abs((L/Lprev)-1) < sensitive;				%%si l'amélioration n'est pas flagrante par rapport à la précédente itération
					end%while
					 
					if(okb)
                        			nbMod=nbMod-1;							%si il n'y avait pas eu d'amélioration significative on rebaisse le nombre de mode
						if(fixe==0)							%%on refait une recherche de gaussienne vu que nbMod a diminué
							[W,M,V,L] = EM_GM_fast(data,nbMod,[], [],0,[]);		
						else	
							t=zeros(1,1,nbMod);					
							for i=1:nbMod
								t(i)=0.001;
							end
							[W,M,V,L] = EM_GM_fast(data,nbMod,[],[],0,struct('W',repmat([1/nbMod],1,nbMod),'M',m(fin),'V',t));	
						end			
						M=sort(M(1,:));
                   			end

					%if nbMod==1								%%un seul mode detecté au final, on ne devrait rien faire puisque l'on appel k-mean en check up final
					%	'call kmeans'
					%	[T,edge,center] = discret_by_kmeans(data,5);
					%else
					%	[W,M,V,L] = EM_GM_fast(data,nbMod,[], [],0,[]);			%%on refait une recherche de gaussienne si on a diminué nbMod (on ne devrai le faire que si okb)
					%	M=sort(M(1,:));
					%end
				%%%%%%%%%%%%%%%%%%%%
				elseif mode==1 									%%detection simple par pic mode 1
					M=sort(m(fin));

				else
					error(['unrecognized mode ' mode]);

               			end
                		%%%%%%%%%%%%%%%%%%%%
				if nbMod>1						%%vérification finale
					M=sort(M);					%%classe mes moyennes
					center=M;
					for k=1:(length(M)-1)
						edge=[edge (M(k)+M(k+1))/2];		%%calcule les limites des bins entre deux moyennes
					end%for k
					edge=sort(edge);				%%ne sert a rien si les moyenens on été classées
				else
					%'call kmeans2'
					[T,edge,center] = discret_by_kmeans(data,5);
				end
		end%if kmeans
	end	
	edge=edge+0.00000000001;		%%modifie legèrement les bins pour avoir une classe 0
	[n,T] = histc_ic(data,edge);		%%effectue la discrétisation suivant les bins
display(edge);
end%function



function m=maximum(vect)
	m=-inf;
	for i=1:length(vect)
		if(vect(i)>m)
		m=vect(i);
	end
end

end
