function [T,edge,center] = discret_by_kmeans(init,class)
% discrétise un tableau de val continues en 3 valeurs en prenant les seuils donnés par kmeans
%pour un k centres
%INPUT: init=tableau de val ou chaque colonnes représente un gène et chaque ligne un sample
%	k=nombre de centres recherchés par l'algo des kmeans(superieur à 3)
%OUTPUT: T=tableau discrétisé
%	 edge=représente pour chaque gène les bornes supérieures et inférieures
%	 center=représente pour chaque gène les centroides des classes trouvées par kmeans  

%protection si pas assez de variation pour faire 3 classes (on sucre les sous exprimés)
%assure au moins 5% de sous exprimé et de sur exprimé mais moins de 33%

[nbE nbG]=size(init);
T=[];
seuilI=0.05; %indique la proportion d'exemple mini a classer dans la classe 1
seuilS=0.05; %%indique la proportion d'exemple mini a classer dans la classe 3
center=0;
if(class<3)
 fprintf('Nombre de classes recherchées au minimum de 3 \n');

else

classA=0;




		if variation(init)<=5
			[val,M]=variation(init);
			ok=1;
			nbMod=val;
			M=sort(M);
			if nbMod>1
				edge=[];
				for k=1:(length(M)-1)
					edge=[edge ((M(k)+M(k+1))/2)];
				end%for k
				edge=sort(edge);
			else 
				edge=[init(1)];
			end
			if nbG==1
				edge=[init(1)];
			end
		else


			classA=min(class,variation(init));
			[idx, centersG] = kmeans3(init,classA);
			center=sort(centersG); %classe les centre de chaque classe
			wrn=0;

			%Modification pour les variations faibles
			if variation(init)<3 %cas d'une valeur qui ne varie pas suffisament pour faire 3 classes
				wrn=1;
			end

			nbSup=1;
			nbInf=1;
			done=0;

			while nbInf<classA & ~done
			temp=0;
			k=1;

				while k<=nbE & ~done
					if init(k)<=((center(nbInf)+center(nbInf+1))/2)
						temp=temp+1;
					end
					if temp>(nbE*seuilI) %classe au moins 5% des exemples dans les classes extrem
						done=1;
						if ~wrn && size(find(init<=(center(nbInf)+center(nbInf+1))/2),1)>nbE/3 %classe 1 trop grande
							nbInf=nbInf-1;
						end
						if wrn && size(find(init<=(center(nbInf)+center(nbInf+1))/2),1)>nbE/2 %classe 1 trop grande
							wrn=-1;
						end
					end
					k=k+1;
				end%while k

				if ~done
					nbInf=nbInf+1;
				end

			end%while nbInf

			if classA<=1
			nbInf=0;
			end

			if nbInf~=0
				cent=0;
				%for j=1:nbInf%fait la moyenne de tout les centroides des classes regroupées
				%	cent=cent+center(j);
				%end
				%binf=cent/nbInf;
				binf=(center(nbInf)+center(nbInf+1))/2;
			else %cas ou il n'y a pas de classe 1
				binf=-inf;

			end
			done=0;

			while nbSup<classA & ~done
				temp=0;
				k=1;

				while k<=nbE & ~done
					if init(k)>=((center(classA+1-nbSup)+center(classA-nbSup))/2)
						temp=temp+1;
					end
					if temp>(nbE*seuilS)
						done=1;
						if size(find(init>=((center(classA+1-nbSup)+center(classA-nbSup))/2)),1)>nbE/3
							nbSup=nbSup-1;
						end
					end
					k=k+1;
				end%while

				if ~done
					nbSup=nbSup+1;
				end

			end
if nbSup==classA 
    nbSup=nbSup-1; 
end
			if nbSup~=0
				cent=0;
				%for j=1:nbSup
				%	cent=cent+center(classA(i)+1-j);
				%end
				%bsup=cent/nbSup;
				bsup=(center(classA+1-nbSup)+center(classA-nbSup))/2;			
			else %cas ou il n'y a pas de classe 3
					bsup=inf;

			end




			%binf=(center(1)+center(2))/2;
			%bsup=(center(class)+center(class-1))/2;

			if wrn
				if wrn==1
					bsup=inf;
				else
					binf=-inf;
				end
			end


			if binf==bsup
				bsup=inf;
			end
			%%rajout pour decaler le bivalué 2,3 vers 1,2
			if binf==-inf
				binf=bsup;
				bsup=inf;
			end


			edge=[binf+1e-7 bsup-1e-7];


		end%if

		
	[n,T] = histc_ic(init,edge); %classe tout les exemples en fonction de leur appartenance


end%if

end%function

