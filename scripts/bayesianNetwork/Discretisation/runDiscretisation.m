function [dataD] = runDiscretisation(dataI,dataO,nbModMax)
%%function [dataD] = runDiscretisation(dataI,dataO,nbModMax)
%%effectue la discrétisation adaptative sur les données contenue dans le fichier dataI et écrit le résultat dans le fichier dataO
%%INPUT : dataI: nom du fichier contenant les données continues (exemples en ligne/variables en colonne)
%%	  dataO: nom du fichier de sortie pour les données discrétisées
%%	  nbModMax: nombre d'etat max pour les données discrétisées (en général 4)
%%OUTPUT : dataD matrice discrétisée



data=load(dataI);
[nbSample,nbNode]=size(data);

dataD=zeros(nbSample,nbNode);

for i=1:nbNode
	if var(data(:,i),1)<10e-05
		disp(['gene '  num2str(i) ' not discretized due to its low variance']);
		dataD(:,i)=ones(nbSample,1);
	else
		dataD(:,i)=discret_adaptative(data(:,i),'nbModMax',nbModMax);
		disp(['gene '  num2str(i) ' discretized']);
	end

end

dlmwrite(dataO,dataD,' ');

end

