#!/bin/tcsh

# Usage: discretization.sh samples_with_header.txt max_nb_discrete_values

# output in file samples_with_header.res

matlab -nodesktop -singleCompThread -sd $0:h -r 'runDiscretisation("../'$1'","../'${1:r}.res'", '$2');quit'

