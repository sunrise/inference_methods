function [var,pres]=variation(M)
%retourne un vecteur contenant le nombre de valeur discrete différente
%contenu dans chaque colonne de M

[l,c]=size(M);

var=zeros(1,c);

for i=1:c

count=0;
pres=[];
for j=1:l


if size(find(pres==M(j,i)),1)==0
pres=[pres ;M(j,i)];
count=count+1;
end
end

var(1,i)=count;
end
