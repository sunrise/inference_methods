# Network inference with Bayesian network method, using two types of data: gene expression data and aggregated snp data for different genotypes.

Different values of equivalent sampling size BDeu parameter alpha are tested, and a bootstrap of the data is performed.
The Bayesian network method is executed at the same time for the snp and expression data.

## Main script: [genebayesnet.sh](./scripts/bayesianNetwork/genebayesnet.sh)

Parameters to set:
 * $1: Name of the expression file (see artificialDataSet_genes.tsv example, each line containing the expression levels for one gene and for all the genotypes)
 * $2: Name of the haplotype file (see artificialDataSet_markers.tsv example, assuming one haplotype, i.e., a tri-allelic marker in {0,1,2}, per gene IN THE SAME ORDER AS IN THE PREVIOUS FILE)
 * $3: Name of the file containing the indices for the different bootstraps (see artificialDataSet_bootstraps.tsv example, each tabulated text line contains a bootstrap identifier bXXX followed by the list of selected genotypes) 

[genebayesnet.sh](./scripts/bayesianNetwork/genebayesnet.sh) uses intermediate scripts : 
  * [genebayesnet.sh](./scripts/bayesianNetwork/genebayesnet.sh) : main script (TIMELIMIT and PARALLEL constants can be modified by the user, default values are 1200 seconds and 10 parallel executions)
  * [matlab.sh](./scripts/bayesianNetwork/matlab.sh) : run the expression level discretization method
  * [Discretisation](./scripts/bayesianNetwork/Discretisation) : matlab source code directory for the discretization method
  * [gobnilp.sh](./scripts/bayesianNetwork/gobnilp.sh) : generate the Bayesian network exact structure learning problem using gobnilp
  * [gobnilp163.linux.x86_64.gnu.opt.cpx](./scripts/bayesianNetwork/gobnilp163.linux.x86_64.gnu.opt.cpx) : gobnilp linux version 1.6.3, including SCIP v3.2.1 integer programming solver (Cussens et al., JAIR 2017)
  * [gobnilp_settings_constraints_alpha.txt](./scripts/bayesianNetwork/gobnilp_settings_constraints_alpha.txt) : gobnilp parameter file
  * [elsa.sh](./scripts/bayesianNetwork/elsa.sh) : main Shell script for Bayesian network structure learning method with two CPU time limits for MINOBS (MINOBSCPUTIME) and ELSA (TIMELIMIT)
  * [elsa](./scripts/bayesianNetwork/elsa) : internal elsa script to executable to run successively MINOBS and ELSA solvers
  * [search](./scripts/bayesianNetwork/search) : MINOBS local search algorithm (Beek and Lee @ 2017)
  * [searchXXX](./scripts/bayesianNetwork/searchXXX) : ELSA complete search algorithm using MINOBS result as initial solution (Tr�sser, Givry, and Katsirelos, IJCAI 2021)
  * [elsa2sol.awk](./scripts/bayesianNetwork/elsa2sol.awk) : convert solutions found by ELSA solver using original variable names
  * [boot2edges.awk](./scripts/bayesianNetwork/boot2edges.awk) : construct a consensus graph with parentset domains from all bootstrap data for a given alpha
  * [boot2jkl_ub.awk](./scripts/bayesianNetwork/boot2jkl_ub.awk) : remove parentset domains having a poorly supported edge (in less than BOOTSTRAPTHRESHOLD bootstraps)
  * [reducefrombootstraps.sh](./scripts/bayesianNetwork/reducefrombootstraps.sh) : main Shell script for parentset domain removals (see above)
  * [makebootstrap.sh](./scripts/bayesianNetwork/makebootstrap.sh) : create bootstrap files by randomly selecting individuals with reinsertion
  * [makebootstrap.py](./scripts/bayesianNetwork/makebootstrap.py) : internal bootstrap procedure
  * [shufcol.py](./scripts/bayesianNetwork/shufcol.py) : randomly shuffle genes (columns) in dat file
  * [constraints.awk](./scripts/bayesianNetwork/constraints.awk) : AWK script to generate a list of unrelevant edges for gobnilp
  * [precision_recall.sh](./scripts/bayesianNetwork/precision_recall.sh) : draw precision/recall curve when the true network is known
  * [precision_recall.plot](./scripts/bayesianNetwork/precision_recall.plot) : internal drawing gnuplot script
  * [parallel.sh](./scripts/bayesianNetwork/parallel.sh) : simple parallelization method on PARALLEL cores, used to parallelize discretization per gene and network inference per bootstrap and per alpha
  
## Required softwares

`matlab`, `gobnilp` (James Cussens @ 2018, see https://www.cs.york.ac.uk/aig/sw/gobnilp/) and `elsa` (Fulya Tr�sser et al @ IJCAI'2021). A working Linux of `gobnilp` and `elsa` is contained in the repository.

## Produced files are in

	* result/constraints.txt					: temporary list of forbidden directed edges for gobnilp
	* result/gobnilp_settings_constraints_YYY.txt			: temporary gobnilp setting file with alpha parameter fixed (YYY)
	* result/$1_Boot_XXX.tsv/.txt/.dat				: temporary bootstrapped gene expression file (bootstrap index XXX, discretization result in .dat)
	* result/$1_Boot_XXX_transp.txt/.dat				: temporary bootstrapped gene expression file transposed from lines to columns
	* result/$2_Boot_XXX.tsv/.txt					: temporary bootstrapped haplotype file
	* result/$2_$1_Boot_XXX.dat					: temporary bootstrapped haplotype and discretized gene file (main input data for gobnilp using MZZZ/EZZZ names internally)
	* result/$2_$1_Boot_XXX_YYY.jkl					: temporary parentset domains for Bayesian network structure learning (output of gobnilp used as input for elsa and minobs)
	* result/$2_$1_Boot_XXX_YYY.res					: temporary result file by gobnilp per bootstrap XXX and per alpha YYY (raw output)
	* result/$2_$1_Boot_XXX_YYY.sif					: temporary result file by gobnilp (list of directed edges of the learned Bayesian network)
	* result/$2_$1.res						: temporary aggregated result file with internal MZZZ/EZZZ names
	* result/$2_$1.txt						: result file with predicted directed edges sorted by decreasing confidence score (edge frequency) (warning, the same edge may be repeated twice with diffent scores if genes and haplotypes have the same name)
	* result/$2_$1_markertogeneonly.txt				: result file with sorted predicted directed edges from markers to genes only
	* result/$2_$1_genegeneonly.txt					: result file with sorted predicted directed edges from genes to genes only
