
# Extract candidate edges which are supported by at least THRESHOLD % bootstrapped score files

# Usage:
# awk -f ./boot2edges.awk -v THRESHOLD=0.1 result/dataTest_expression_Boot_*_*.jkl | sort -g -k 2,2 -k 1,1 > result/dataTest_expression_Boot_edges.txt

BEGIN{
	target=-1;
	parents[-1]=0;
}

FNR==1{
	n++;
	p = 0;
}

FNR>1 {
  if (p>0) {
	for (i=3;i<=2+$2;i++) {
		parents[$i]++
	}
	p--;
  } else {
	if (target>=0) {
		for (source in parents) {
			edges[source " " target]++;
		}
	}
	target=$1;
	p=$2;
	delete parents;
  }
}

END{
	if (target>=0) {
		for (source in parents) {
			edges[source " " target]++;
		}
	}
	for (e in edges) {
		if (edges[e]/n > THRESHOLD) {
			print e;
		}
	}
}
