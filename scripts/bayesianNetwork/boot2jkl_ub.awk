# Transform a score file by removing parentsets such that at least one parent is not supported by bootstrapped candidate edges (see boot2edges.awk)

# Usage:
# awk -f ./boot2jkl_ub.awk result/dataTest_expression_Boot_edges.txt result/dataTest_expression_Boot_11_1_.jkl > result/dataTest_expression_Boot_11_1.jkl

# needs an input list of allowed candidate edges that will pass the bootstrap procedure test (see boot2edges.awk)

BEGIN {
	target = -1;
	sets = "";
	n = 0;
}

FNR==NR{
	edges[$1 "_" $2]=1;
}

NR>FNR && FNR==1 {
	print $0;
	p = 0;
}

NR>FNR && FNR>1 {
  if (p==0) {
	if (target >=0) {
		print target " " n "" sets;
	}
	target=$1;
	p = $2;
	n = 0;
	sets = "";
  } else {
	ok=1;
	for (i=3;i<=2+$2;i++) {
		if (($i "_" target in edges)==0) {
			ok=0;
			break;
		}
	}
	if (ok) {
		sets = sets "\n" $0;
		n++;
	}
        p--;
  }
}

END {
	if (target >=0) {
		print target " " n "" sets;
	}
}
