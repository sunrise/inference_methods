
BEGIN {
  p = 0+ARGV[1];
  for (i=1; i<=p; i++) {
    for (j=1; j<=p; j++) {
      print "~M" i "<-E" j;
    }
  }
  for (i=1; i<=p; i++) {
    for (j=i+1; j<=p; j++) {
      print "~M" i "<-M" j;
      print "~M" j "<-M" i;
    }
  }
  exit(0);
}
