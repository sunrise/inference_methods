#!/bin/tcsh

limit cputime $5

set datfile=`echo -n "${1:r}" | sed -E 's/_[0-9e-]+$/.dat/'`
set seed=`echo -n "${datfile:r}" | sed -E 's/^.*_//'`
awk 'FNR==NR&&FNR==1{for (i=1;i<=NF;i++) idx[$i]=i-1} FNR\!=NR{for (i=1;i<=NF;i++) if ($i in idx) $i=idx[$i]; print $0}' $datfile $1 > ${1:r}_.jkl
elsa -t $2 -m $3 -B $4 -s $seed ${1:r}_.jkl > ${1:r}.out
rm -f ${1:r}_.jkl
awk -f ./elsa2sol.awk ${1:r}.out > ${1:r}_.res
awk 'FNR==NR&&FNR==1{for (i=1;i<=NF;i++) name[i-1]=$i} FNR\!=NR{for (i=1;i<NF;i++) if ($i in name) $i=name[$i]; print $0}' $datfile ${1:r}_.res > ${1:r}.res
rm -f ${1:r}_.res

