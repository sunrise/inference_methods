# extract solution (Bayesian network DAG structure) from elsa output

FNR==1{N=int(($3)/2)} 

/minimum cost/||/Total Score:/{ok=0} 

ok&&/<--/{
  sub("ordering[[][0-9]+[]][\t ]+=[\t ]*","",$0);
  sub("<--","<-",$0);
  gsub("[{}]","",$0);
  sub("depth =","",$0);
  bn=bn"\n"$0;
} 

ok&&/Ordering/&&$6=="Parents:"{
  sub("Ordering[[][0-9]+[]][\t ]+=[\t ]*","",$0);
  sub("Score:.*Parents:","<-",$0);
  gsub("[{}]","",$0);
  sub("Valid:","",$0);
  bn=bn"\n"$0;
}

/upper bound solution/||/backtracking solution/||/Generations:/{bn="";ok=1} 

END{print bn}
