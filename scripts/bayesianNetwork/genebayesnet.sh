#!/bin/tcsh

# I assume two (three) input files in tsv format as parameters:
# $1: gene expression data
# $2: genotype data
# $3: bootstrap data (optional)
if (($# < 2)) then
	echo "missing arguments: ./genebayesnet.sh gene_expression_datafile genotype_datafile (bootstrap_datafile)"
	exit 0
endif

# number of CPU cores available to use in parallel mode and maximum CPU time limit (seconds) for structure learning
# (total running time will be multiplied by the number of bootstraps and parameter ESS/alpha settings)
# for large problems with more than 300 genes, increase MINOBSCPUTIME to 900 seconds
# for very large problems with more than 1,000 genes (like DREAM5), optimality proofs cannot be obtained, only local search can be done, increase TIMELIMIT to 3600 and MINOBSCPUTIME to 3600 seconds (see also extra time and alpha values)

set PARALLEL=10
set TIMELIMIT=1200
set MINOBSCPUTIME=300

if (($MINOBSCPUTIME > $TIMELIMIT)) then
	echo "MINOBSCPUTIME ($MINOBSCPUTIME) must be smaller than TIMELIMIT ($TIMELIMIT)!"
	exit -1
endif

set SCIPLIMIT=$TIMELIMIT
set ELSACPUTIME=$TIMELIMIT
@ ELSACPUTIME -= $MINOBSCPUTIME

# extra time to avoid premature stop by the shell (increase this value to 100 for very large problems such as DREAM5)
@ TIMELIMIT += 10

# See example datafiles for their format and try:
# ./genebayesnet.sh exampleData_expression.tsv exampleData_haplotype.tsv exampleData_bootstrap.tsv

# Discretisation software in MATLAB
# Bayesian network structure learning softwares gobnilp (James Cussens @ 2018), minobs (Beek and Lee @ 2017), elsa (Tr�sser et al @ 2021)

set genes=${1:t:r}
set markers=${2:t:r}

set NBBOOTSTRAP=1
set BOOTSTRAPTHRESHOLD="0.3333"
set ALPHAMINEXP=20
# for very large problems (e.g. DREAM5), decrease the following value to 1
set ALPHAMAX=5
set PARENTLIMIT=2
set DISCRETISATION=3
set PARTITIONMAX=10

set expsmallalpha=`awk 'BEGIN{for (i='$ALPHAMINEXP';i>=1;i--) printf(" 1e-%d 5e-%d",i,i);print ""}'`
set normalalpha=`awk 'BEGIN{for (i=1;i<='$ALPHAMAX';i++) printf(" %g",i);print ""}'`
if (($# == 3)) then
	set NBBOOTSTRAP=`wc -l $3 | awk '{printf("%d",$1)}'`
endif

### beginning of main process starts here, can be skipped partially if warm restart needed
#if ((0)) then
## skipped part...
#endif

echo "Build resulting files in directory "`pwd`"/result"
mkdir result

if (($# == 3)) then
	# build bootstrap expression files
	echo "Build $NBBOOTSTRAP bootstrap datafiles.."
	set b=1
	while (($b <= $NBBOOTSTRAP))
		awk 'BEGIN{n=0;ok=1} ok&&FNR==NR&&(($1=="b'$b'")||($1=="bootstrap'$b'")){n=NF-1;for (j=2;j<=NF;j++) indiv[j-1]=$j;ok=0} FNR\!=NR&&FNR==1{gsub("\"","",$0); for (j=1;j<=NF;j++) order[$j]=j; for (j=1;j<n;j++) printf("\"%s\"\t",indiv[j]); print "\"" indiv[n] "\"";} FNR\!=NR&&FNR>1{printf("%s",$1); for (j=1;j<=n;j++) printf("\t%s",$(1+order[indiv[j]])); print ""}' $3 $1 > result/${genes}_Boot_${b}.tsv
		@ b += 1
	end

	# build bootstrap marker files
	set b=1
	while (($b <= $NBBOOTSTRAP))
		awk 'BEGIN{n=0;ok=1} ok&&FNR==NR&&(($1=="b'$b'")||($1=="bootstrap'$b'")){n=NF-1;for (j=2;j<=NF;j++) indiv[j-1]=$j;ok=0} FNR\!=NR&&FNR==1{gsub("\"","",$0); for (j=1;j<=NF;j++) order[$j]=j; for (j=1;j<n;j++) printf("\"%s\"\t",indiv[j]); print "\"" indiv[n] "\"";} FNR\!=NR&&FNR>1{printf("\"%s\"",$1); for (j=1;j<=n;j++) printf("\t%s",$(1+order[indiv[j]])); print ""}' $3 $2 > result/${markers}_Boot_${b}.tsv
		@ b += 1
	end
	echo "..done"
else
	cp $1 result/${genes}_Boot_1.tsv
	cp $2 result/${markers}_Boot_1.tsv
endif

# convert into text files without header nor variable identifier
foreach f (result/${genes}_Boot_*.tsv result/${markers}_Boot_*.tsv)
	awk 'FNR>1{$1="";sub("^ *","",$0);gsub("\t"," ",$0);print $0}' $f > ${f:r}.txt
end

# transpose matrices in order to have individuals in line and variables in column
foreach f (result/${genes}_Boot_*.txt result/${markers}_Boot_*.txt)
	awk '{p++;n=NF;for (i=1;i<=n;i++) mat[p,i]=$i} END{for (j=1;j<=n;j++) {printf("%s",mat[1,j]);for (i=2;i<=p;i++) printf(" %s",mat[i,j]);print ""}}' $f > ${f:r}_transp.txt
end

# discretise gene expressions into DISCRETISATION values (requires MATLAB!!!)

echo "Discretisation of gene expressions in $DISCRETISATION values.."
#uncomment is not using parallel mode
#foreach f (result/${genes}_Boot_*_transp.txt)
#	matlab -nodesktop -singleCompThread -sd ./Discretisation -r 'runDiscretisation("../'$f'","../'${f:r}.res'", '$DISCRETISATION');quit'
#end
./parallel.sh -j $PARALLEL -r "./Discretisation/runDiscretisation.sh * $DISCRETISATION" result/${genes}_Boot_*_transp.txt >& /dev/null
echo "..done"

# add a basic header with gene/marker names
foreach f (result/${genes}_Boot_*_transp.res )
	awk 'FNR==1{printf("E1"); for (i=2;i<=NF;i++) printf(" E%d",i); print ""} {print $0}' $f > ${f:r}.dat
end
foreach f (result/${markers}_Boot_*_transp.txt )
	awk 'FNR==1{printf("M1"); for (i=2;i<=NF;i++) printf(" M%d",i); print ""} {print $0}' $f > ${f:r}.dat
end

# concatenate discretised gene expression and marker data and shuffle columns
echo "Concatenate discretised gene expression and marker data and shuffle columns"
set b=1
while (($b <= $NBBOOTSTRAP))
	awk 'BEGIN{f=0;n=0} FNR==1{f++;file[f]=FILENAME} {line[FILENAME,FNR]=$0;if(FNR>n)n=FNR} END{for (l=1;l<=n;l++) {printf("%s",line[file[1],l]);for (i=2;i<=f;i++) printf(" %s",line[file[i],l]); print ""}}' result/${markers}_Boot_${b}_transp.dat result/${genes}_Boot_${b}_transp.dat > result/${markers}_${genes}_Boot_${b}_.dat
	python3 shufcol.py result/${markers}_${genes}_Boot_${b}_.dat result/${markers}_${genes}_Boot_${b}.dat $b
	rm -f result/${markers}_${genes}_Boot_${b}_.dat
	@ b += 1
end
echo "..done"

# generate constraints on biologically forbidden edges Ei->Mj and Mi->Mj
set p=`wc -l $1`
@ p -= 1
echo "Disallow biologically forbidden edges from expressions to markers and between markers for $p variables.."
awk -f ./constraints.awk $p > result/constraints.txt
echo "..done"

# create gobnilp setting files varying the BDeu Equivalent Sampling Size (alpha)
foreach alpha ($expsmallalpha $normalalpha)
	sed "s/ALPHA/${alpha}/" ./gobnilp_settings_constraints_alpha.txt | sed "s/PARENTLIMIT/${PARENTLIMIT}/" | sed "s/SCIPLIMIT/${SCIPLIMIT}/" > result/gobnilp_settings_constraints_${alpha}.txt
end

# run gobnilp for all bootstraps and all effective sample size values (ESS/alpha value) to generate potential parentset domains in jkl format
echo "Run gobnilp exact Bayesian network structure learning algorithm for each bootstrap and alpha value.."

#uncomment is not using parallel mode
#echo -n "" > result/rungobnilp.sh
#foreach alpha ($expsmallalpha $normalalpha)
#	foreach f (result/${markers}_${genes}_Boot_*.dat)
#		echo "./gobnilp.sh ${alpha} $f ${TIMELIMIT}" >> result/rungobnilp.sh
#	end
#end
#chmod 755 result/rungobnilp.sh
#./result/rungobnilp.sh

if (($# == 3)) then
	foreach alpha ($expsmallalpha $normalalpha)
		./parallel.sh -j $PARALLEL -r "./gobnilp.sh ${alpha} * ${TIMELIMIT}" result/${markers}_${genes}_Boot_*.dat
	end
else
	./parallel.sh -j $PARALLEL -r "./gobnilp.sh * result/${markers}_${genes}_Boot_1.dat ${TIMELIMIT}" $expsmallalpha $normalalpha
endif
echo "..done"

# remove unfinished (timeout) jkl files
echo "Remove unfinished (timeout) parentset domain files (.jkl)"
\rm -f `find result -name "${markers}_${genes}_Boot_*_*.jkl" -exec awk 'BEGIN{n=0;t=0;u=0} FNR==1{n=0;t=$1;u=0} FNR>1&&n==0{u++;n=1+$2} FNR>1&&n>0{n--} END{if (t\!=u || n\!=0) print FILENAME}' {} \;`
echo "..done"

# reduce parentset domains by removing edges not supported by at least 1/3 of number of bootstraps
if (($# == 3)) then
	echo "Reduce parentset domains by removing edges not supported by at least ${BOOTSTRAPTHRESHOLD}*${NBBOOTSTRAP} bootstraps.."
	set TOTALSIZEBEFORE=`wc -l result/${markers}_${genes}_Boot_*_*.jkl | awk '$2=="total"{printf("%d",$1)}'`

#uncomment is not using parallel mode
#	foreach alpha ($expsmallalpha $normalalpha)
#          set nbfiles=`ls result/${markers}_${genes}_Boot_*_${alpha}.jkl | wc -l | awk '{printf("%d",$1)}'`
#          if (($nbfiles > 0)) then
#		awk -f ./boot2edges.awk -v THRESHOLD=${BOOTSTRAPTHRESHOLD} result/${markers}_${genes}_Boot_*_${alpha}.jkl | sort -g -k 2,2 -k 1,1 > result/${markers}_${genes}_${alpha}_validedges.txt
#		foreach f (result/${markers}_${genes}_Boot_*_${alpha}.jkl)
#			mv $f ${f:r}_.jkl
#			awk -f ./boot2jkl_ub.awk result/${markers}_${genes}_${alpha}_validedges.txt ${f:r}_.jkl > $f
#			rm -f ${f:r}_.jkl
#		end
#          endif
#	end
        ./parallel.sh -j $PARALLEL -r "./reducefrombootstraps.sh * result/${markers}_${genes} ${BOOTSTRAPTHRESHOLD}" $expsmallalpha $normalalpha

	set TOTALSIZE=`wc -l result/${markers}_${genes}_Boot_*_*.jkl | awk '$2=="total"{printf("%d",$1)}'`
	set REDUCFACTOR=`awk 'BEGIN{printf("%g",'"${TOTALSIZEBEFORE}"'/'"${TOTALSIZE}"')}'`
	echo "Total parentset domains: ${TOTALSIZE} (${REDUCFACTOR} reduction factor)"
	echo "..done"
endif

# run elsa for all bootstraps and all effective sample size values (ESS/alpha value) to build Bayesian networks

echo "Run elsa solver with MINOBS local search for each bootstrap and alpha value.."
./parallel.sh -j $PARALLEL -r "./elsa.sh * ${ELSACPUTIME} ${MINOBSCPUTIME} ${PARTITIONMAX} ${TIMELIMIT}" result/${markers}_${genes}_Boot_*_*.jkl
echo "..done"

# extract networks in sif format from the results
echo "Extract Bayesian network edges in sif format.."
foreach f (result/${markers}_${genes}_Boot_*.res)
  awk '/<-/{$NF="";sub("<-"," ",$0);gsub(","," ",$0);for (i=2;i<=NF;i++) print $i,"ac",$1}' $f > ${f:r}.sif
end

# delete empty networks
find result -empty -print -exec rm {} \; > /dev/null
echo "..done"

# count how many networks have been found within the given time limit
set NBOPTIMUM=`grep "total time =" result/${markers}_${genes}_Boot_*.out | wc -l | awk '{printf("%d",$1)}'`
set NBELSAIMPROVE=`find result -name "${markers}_${genes}_Boot_*.out" -exec awk '/(LP)|(GAC) primal/{print FILENAME,$0;exit}' {} \; | wc -l`
set NBNETWORK=`ls result/${markers}_${genes}_Boot_*.sif | wc -l | awk '{printf("%d",$1)}'`
set NBALPHA=`echo "$expsmallalpha $normalalpha" | wc -w | awk '{printf("%d",$1)}'`
set NBTOTAL=`awk 'BEGIN{printf("%d", '"${NBALPHA}"'*'"${NBBOOTSTRAP}"')}'`
echo "Found ${NBOPTIMUM} optimal (${NBELSAIMPROVE} improved by Elsa) (${NBNETWORK} produced by MINOBS) networks for a total of ${NBTOTAL} runs." 

# output edge frequencies (gene regulations with a confidence score)
echo "Output edge list with associated frequencies (gene/marker regulations with a confidence score) in file "`pwd`"/result/${markers}_${genes}.txt"
awk 'BEGIN{n=0} NF==3{src=$1;sub("M","E",src); if (src != $3) edge[$1 " " $3]++;file[FILENAME]=1} END{for (f in file) n++; for ( e in edge ) print e,edge[e]/n}' result/${markers}_${genes}_Boot_*.sif | sort -r -g -k 3,3 > result/${markers}_${genes}.res
awk 'FILENAME==ARGV[1]&&FNR>1{name["E"FNR-1]=$1} FILENAME==ARGV[2]&&FNR>1{name["M"FNR-1]=$1} FILENAME==ARGV[3]{$1=name[$1];$2=name[$2];print $0}' $1 $2 result/${markers}_${genes}.res > result/${markers}_${genes}.txt

grep "M" result/${markers}_${genes}.res > result/${markers}_${genes}_markertogeneonly.res
grep -v "M" result/${markers}_${genes}.res > result/${markers}_${genes}_genegeneonly.res
awk 'FILENAME==ARGV[1]&&FNR>1{name["E"FNR-1]=$1} FILENAME==ARGV[2]&&FNR>1{name["M"FNR-1]=$1} FILENAME==ARGV[3]{$1=name[$1];$2=name[$2];print $0}' $1 $2 result/${markers}_${genes}_markertogeneonly.res > result/${markers}_${genes}_markertogeneonly.txt
awk 'FILENAME==ARGV[1]&&FNR>1{name["E"FNR-1]=$1} FILENAME==ARGV[2]&&FNR>1{name["M"FNR-1]=$1} FILENAME==ARGV[3]{$1=name[$1];$2=name[$2];print $0}' $1 $2 result/${markers}_${genes}_genegeneonly.res > result/${markers}_${genes}_genegeneonly.txt

echo "..done"

