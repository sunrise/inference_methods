#!/bin/tcsh

limit cputime $3

sed "s/PROBLEM/${2:r:t}_${1}/" result/gobnilp_settings_constraints_${1}.txt > result/gobnilp_settings_constraints_${2:r:t}_${1}.txt
./gobnilp163.linux.x86_64.gnu.opt.cpx -x -g=result/gobnilp_settings_constraints_${2:r:t}_${1}.txt -f=dat $2 > /dev/null
rm -f result/gobnilp_settings_constraints_${2:r:t}_${1}.txt
