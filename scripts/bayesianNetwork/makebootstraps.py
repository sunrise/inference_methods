import sys
import pandas as pd

inputfile = sys.argv[1]
outputfile = sys.argv[2]
nbbootstraps = int(sys.argv[3])

df = pd.read_csv(inputfile, sep='\t', header=None, nrows=1)
dfT = df.T
for seed in range(nbbootstraps):	
	chuf = dfT.sample(frac=1, random_state=seed, replace=True)
	chufT = chuf.T
	chufT.to_csv(outputfile+'-'+str(seed)+'.tsv', index=False, sep='\t')

