#/bin/tcsh

# Generate a file of bootstrapped individual names

# Usage:
# ./makebootstraps.sh exampleData_expression.tsv exampleData_bootstrap.tsv 50

python3 makebootstraps.py $1 ${2:r} $3
awk 'FNR==2{b++;print "b"b,$0}' ${2:r}-*.tsv | sed 's/ /\t/g' > $2
rm -f  ${2:r}-*.tsv

