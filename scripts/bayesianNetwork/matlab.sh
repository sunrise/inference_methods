#!/bin/tcsh

matlab -nodesktop -singleCompThread -sd ./Discretization -r 'runDiscretisation("../'$1'","../'${1:r}.res'", '$2');quit'
