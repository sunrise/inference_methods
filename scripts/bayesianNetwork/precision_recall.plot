#!/usr/bin/gnuplot

set key top right
set xlabel 'Recall'
set ylabel 'Precision'

plot './result/pr.txt' u 1:2 w line  lc 1 lw 2 lt 1 t 'oriented', './result/prno.txt' u 1:2 w line  lc 2 lw 2 lt 2 t 'not oriented'

pause -1
set term postscript eps enhanced color 20
set output 'pr_curves.eps'
replot

