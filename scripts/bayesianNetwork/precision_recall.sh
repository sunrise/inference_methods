#!/bin/tcsh

# Usage: ./precision_recall.sh exampleData_network.tsv ./result/exampleData_haplotype_exampleData_expression.txt

awk '{gsub("\"","",$0)} FNR==NR{edge[$1 ":" $2]=1; e++} FNR\!=NR{gsub("E","G",$0);gsub("M","G",$0)} FNR\!=NR&&\!(($1 ":" $2) in seen){if (($1 ":" $2) in edge) t++; n++; print t/e,t/n; seen[$1 ":" $2]=1}' $1 $2 >! ./result/pr.txt

awk '{gsub("\"","",$0)} FNR==NR{edge[$1 ":" $2]=1; edge[$2 ":" $1]=1; e++} FNR\!=NR{gsub("E","G",$0);gsub("M","G",$0)} FNR\!=NR&&\!(($1 ":" $2) in seen){if (($1 ":" $2) in edge) t++; n++; print t/e,t/n; seen[$1 ":" $2]=1; seen[$2 ":" $1]=1}' $1 $2 >! ./result/prno.txt

gnuplot precision_recall.plot

