#!/bin/tcsh

# ./reducefrombootstraps.sh alpha prefixname bootstrapthreshold

set nbfiles=`ls ${2}_Boot_*_${1}.jkl | wc -l | awk '{printf("%d",$1)}'`
if (($nbfiles > 0)) then
	awk -f ./boot2edges.awk -v THRESHOLD=${3} ${2}_Boot_*_${1}.jkl | sort -g -k 2,2 -k 1,1 > ${2}_${1}_validedges.txt
	foreach f (${2}_Boot_*_${1}.jkl)
		mv $f ${f:r}_.jkl
		awk -f ./boot2jkl_ub.awk ${2}_${1}_validedges.txt ${f:r}_.jkl > $f
		rm -f ${f:r}_.jkl
	end
endif

