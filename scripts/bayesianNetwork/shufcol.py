# shuffle columns 

# usage:
# python3 shufcol.py inputfile outputfile randomseed

import sys
import pandas as pd

inputfile = sys.argv[1]
outputfile = sys.argv[2]
seed = int(sys.argv[3])

df = pd.read_csv(inputfile, sep=' ')

dfT = df.T
shuffled_dfT = dfT.sample(frac=1, random_state=seed)
shuffled_df = shuffled_dfT.T

shuffled_df.to_csv(outputfile, index=False, sep=' ')

