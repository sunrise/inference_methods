# Construction of the genotypes matrix
# For each bootstrap (1:50), as column, gives the genotypes to take (with repetition)

dthap <- read.csv("data/exampleData_haplotype.tsv", sep="\t", row.names=1, header=T)
# We bootstrap on the 463 genotypes
namhap <- colnames(dthap)

bootstrap_matrix <- matrix("", nrow = length(namhap), ncol=50)
colnames(bootstrap_matrix) <- paste0("b", 1:50)
for(i in 1:50){
  bootstrap_matrix[,i] <- sample(namhap, replace = TRUE)
}
bootstrap_matrix <- t(bootstrap_matrix)

if(dir.exists("results/")==F) dir.create("results/", recursive = T)
write.table(bootstrap_matrix, file = "results/bootstrap_matrix.tsv", sep="\t", col.names = F, quote = F)
