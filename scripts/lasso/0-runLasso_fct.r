#' removeExt
#'
#' Remove any common extension from a vector of file names
#' by Gordon Smyth, 19 July 2002.  Last modified 19 Jan 2005.
#' 
#' @param x file name with extention and path
#'
#' @return file name without extension and paths
removeExt <- function(x) {
  x <- as.character(x)
  x <- basename(x)
  n <- length(x)
  ext <- sub("(.*)\\.(.*)$","\\2",x)
  if(all(ext[1] == ext))
    return(sub("(.*)\\.(.*)$","\\1",x))
  else
    return(x)
}


##########################################################################
#### PARAMETERS ##########################################################
##########################################################################

#' Title
#'
#' @param infileExp Link to expression file (tab separated, with header)
#' @param infileSnp Link to snp/haplotype file (tab separated, with header)
#' @param GeneAsRows Are genes in rows (TRUE, default) or not (FALSE)
#' @param dataScale What data to scale ?
#'  1 : only expression data (default)
#'  2 : only haplotype/snp data
#'  3 : both : expresssion + haplotype/snp
#' @param nbLambda Size of lambda vectors
#' @param nbCores Number of cores to use for parallelization. 
#' If NULL (default), use parallel::detectCores() - 1.
#' If 1, the function won't be parallelized. 
#' @param outDir Directory for outputs (the results will be stored in "outDir/outFileName/out_randomForest")
#' @param outFileName Directory for outputs (lats part, usually depending on the name of input files).
#' If NULL (default), it will be the name of expression data file, whithout extension.
#' @param nbBootstrap Number of bootstrap samples
#' @param writeRes Boolean, should the intermediate results also be writen ?
#'
#' @return
#' @export
#'
#' @examples runLasso(infileExp="data/exampleData_expression.tsv",
#'                 infileSnp="data/exampleData_haplotype.tsv", 
#'                 GenesAsRows = TRUE, 
#'                 dataScale=1,
#'                 nbLambda=20)
runLasso <- function(infileExp, infileSnp, GenesAsRows = c(TRUE, FALSE), dataScale = c(1,2,3), 
                     nbLambda = 100, nbCores = NULL, outDir = "RESULTS/", outFileName = NULL,
                     nbBootstrap = 50, writeRes=F) {
  
  if(file.exists(infileExp)==FALSE){
    stop("Expression file doesn't exists")
  }
  if(file.exists(infileSnp)==FALSE){
    stop("Expression file doesn't exists")
  }
  
  lastchar <- substr(outDir, nchar(outDir), nchar(outDir))
  if(lastchar != "/"){
    outDir <- paste0(outDir, "/")
  }
  
  if(is.null(outFileName)){
    outFileName <- paste(removeExt(infileExp), collapse="")
  } else {
    lastchar <- substr(outFileName, nchar(outFileName), nchar(outFileName))
    if(lastchar != "/"){
      outFileName <- paste0(outFileName, "/")
    }   
  }
  if(is.null(nbCores)) nbCores <- detectCores()-1
  
  # Location of output and temporary files
  fileRep <- paste0(outDir, outFileName, "/out_lasso/")

  ##########################################################################
  #### MAIN ################################################################
  ##########################################################################
  
  parameters <<- list("infileExp" = infileExp,
                    "infileSnp" = infileSnp,
                    "outFileName" = outFileName,
                    "nbBootstrap" = nbBootstrap,
                    "nbLambda" = nbLambda,
                    "dataScale" = dataScale,
                    "GenesAsRows" = GenesAsRows,
                    "nbCores" = nbCores,
                    "fileRep" = fileRep)
  
  # creating the repertory if needed
  if(!dir.exists(fileRep)){
    dir.create(fileRep, recursive = T)
  }
  
  # creating the temp repertory if needed
  if(writeRes==T){
    fileRepTemp <- paste0(fileRep, "temp/")
    if(!dir.exists(fileRepTemp)){
      dir.create(fileRepTemp, recursive = T)
    }
  }

  print("# 1/6 - Loading and conversion of data")
  step1 <- fct_lassoConv(parameters, writeRes)
  
  print("# 2/6 - Bootstraping")
  step2 <- fct_lassoBootstrap(step1$parameters, step1$dataExp, step1$dataSnp, writeRes)
  
  print("# 3/6 - Extimate the lambda max for each gene in each bootstrap sample")
  #### Long running step
  step3 <- fct_lassolambdamax(step2$parameters, step2$databoot, writeRes)
  
  print("# 4/6 - Common lambda vector to use for all genes and all bootstrap sample")
  step4 <- fct_lassoLBDVEC(step3$parameters, step3$lambdaMaxVec, writeRes)
  
  print("# 5/6 - Performs the lasso for each gene in each bootstrap using the vector of lambdas")
  step5 <- fct_lassoReg(step4$parameters, step2$databoot, step4$lambdaVec, writeRes)
  
  print("# 6/6 - Combine the results")
  step6 <- fct_lassoRes(step5$parameters, step5$betaReg)
  
}

