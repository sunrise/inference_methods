##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

## Module 2 : Bootstraps Implementation
# Fist perform a scaling of the data, if needed (parameter ** dataScale **)
# Then create the bootstraps samples (parameter ** nbBootstrap **)
# If nbBootstrap=0, no bootstrap is performed.

##########################################################################
#### FUNCTIONS ###########################################################
##########################################################################

#' dataScaling
#'
#' Scale the data : expression and/or haplotype depending on the user's choice
#' 
#' @param exp gene expression data
#' @param snp snp/haplotype data
#' @param dataScale which data to scale :
#' #'          1: expression, 2: snp/haplotype  3: both
#'
#' @return list : exp = expression data, snp : snp/haplotype data
#' @export
dataScaling <- function(exp,snp, dataScale){
  scaledData <- list("exp" = exp,
                     "snp" = snp)
  
  if (dataScale %in% c(1,3)){
    scaledData$exp = scale(exp)
    #rownames(scaledData$exp) = rownames(exp)
    # TODO A confirmer avec Lise
  }
  if (dataScale %in% c(2,3)){
    scaledData$snp = scale(snp)
    #rownames(scaledData$snp) = rownames(snp)
    # TODO A confirmer avec Lise
  }
  
  ## The scale function returns NaN if the variance is 0. 
  ## So we replace the NaN values by 0 if there is any,
  ## so it won't disturb the future network inference
  scaledData$exp[which(is.na(scaledData$exp))] = 0
  scaledData$snp[which(is.na(scaledData$snp))] = 0

  return(scaledData)
}

#' dataSampling
#' 
#' Sample the data, with replacement. 
#' The sample is done on genotype data, and the return data have the 
#' same size as the initial data.
#' The same sample indexes are used to sample SNP data so samples are consistent
#'
#' @param exp données d'expression
#' @param snp données SNP / haplotype
#'
#' @return dataSample : 1 bootstrap des données sur les genotypes
dataSampling <- function(exp,snp){
  genotype = rownames(exp)
  
  genotypeSample <- sample(genotype, replace = TRUE)
  
  bootstrapExp <- exp[genotypeSample,]
  bootstrapSnp <- snp[genotypeSample,]

  return(list(exp = bootstrapExp, snp = bootstrapSnp))
}

##########################################################################
#### MAIN ################################################################
##########################################################################

fct_lassoBootstrap <- function(parameters, dataExp, dataSnp, writeRes=F){

  databoot <- list()
  
  # If no bootstrap
  if (parameters$nbBootstrap == 0){
    databoot <- dataScaling(exp = dataExp, snp = dataSnp, dataScale = parameters$dataScale)
  } else if (parameters$nbBootstrap > 0){ # If bootstrap
    databoot = lapply(X = c(1:parameters$nbBootstrap), function(x) dataSampling(dataExp, dataSnp))
    if(parameters$dataScale>0){
      databoot = lapply(X = databoot, function(x) dataScaling(exp = x$exp, 
                                                                snp = x$snp, 
                                                                dataScale = parameters$dataScale))
    } 
  }
  
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  if(writeRes==T){
    link.resultat <- paste(parameters$fileRep, "/temp/bootdata_nbBoot_", parameters$nbBootstrap, ".Rdata", sep="")
    save(parameters, databoot, file = link.resultat)
    
    print(paste("Object created :", link.resultat))
  }
  return(list("parameters"=parameters, "databoot"=databoot))
}

