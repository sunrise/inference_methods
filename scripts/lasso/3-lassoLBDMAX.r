##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Module 3 : compute the max of lambda (lasso) 
# for each gene in each bootstrap

# Long running step !!  
# If parallel (nbCores>1) : mclapply, else lapply

##########################################################################
#### FUNCTION ############################################################
##########################################################################

#' selectLambdaMax
#'
#'Estimate the lambdaMax of a gene
#'
#' @param idGene id of the gene for which we want the lamdaMax
#' @param variable list of two elements, containing potential explanatory variables
#'          $exp : measurement of gene expression in the bootstrap
#'          $snp : measurement of snp in the bootstrap
#' @return lambdaMax : numeric value, value of lambdaMax for the gene
selectLambdaMax <- function(idGene, variable){
  # indexes of the gene of interest
  iG = which(colnames(variable$exp)==idGene)

  explanatory_vars <- cbind(variable$exp[,-iG],variable$snp)  
  resp_var <- variable$exp[,iG]

  # Run the lasso regression for the IdGene
  out.lars = list("lambda" = c(0))
  out.lars <- glmnet(as.matrix(explanatory_vars), 
                                 resp_var, 
                                 family='gaussian', 
                                 standardize=F, 
                                 alpha=1)
  ## Selection of lambda max
  lambdaMax <- max(out.lars$lambda)
  
  return(lambdaMax)
}

#' selectBootstrapLambdaMax
#' 
#' In a bootstrap sample, estimate the lambdaMax of each gene
#'
#' @param bootstrap list of two vectors
#'          $exp : measurement of gene expression in the bootstrap
#'          $snp : measurement of snp in the bootstrap
#'
#' @return bootstrapLambdaMAx : value of lambdaMax of each gene in the bootstrap
selectBootstrapLambdaMax <-function(bootstrap)
{
  listIdGene = colnames(bootstrap$exp)
  
  bootstrapLambdaMax = unlist(lapply(X = listIdGene, function(x) selectLambdaMax(idGene = x, variable = bootstrap)))
  
  return(bootstrapLambdaMax)
}

##########################################################################
#### MAIN ################################################################
##########################################################################
fct_lassolambdamax <- function(parameters, databoot, writeRes=F){
  
  lambdaMaxVec = mclapply(X = databoot, function(x) selectBootstrapLambdaMax(bootstrap = x), 
                          mc.cores = parameters$nbCores)
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  if(writeRes==T){
    link.resultat <- paste0(parameters$fileRep, "/temp/lambdaMax.Rdata")
    save(lambdaMaxVec, parameters, file = link.resultat)
    
    print(paste("Object created :", link.resultat))
  }
  return(list("parameters"=parameters, "lambdaMaxVec"=lambdaMaxVec))
  
}
