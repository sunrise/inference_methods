##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Module 4 : compute the lambda vector
# From the lambdaMax values, select the max one and 
# generate a lambda vector (from O ???? to lambda) 
# to use in final lasso regression
# (the lambda vector will be the same for all genes)

##########################################################################
#### MAIN ################################################################
##########################################################################
fct_lassoLBDVEC <- function(parameters, lambdaMaxVec, writeRes=F){

  # max of lambdas accross bootstraps
  lambdaMax = max(unlist(lambdaMaxVec))
  
  # Generate a sequence of lambdas to use in final regressions:
  tmpVec <-  as.vector(seq(from = lambdaMax,
                             to = lambdaMax/(parameters$nbLambda+1),
                             length = parameters$nbLambda+1))
  lambdaVec <- tmpVec[-1]
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  if(writeRes==T){
    link.resultat <- paste0(parameters$fileRep, "/temp/LBDvecteur.Rdata")
    save(lambdaVec, parameters, file = link.resultat)
    
    print(paste("Object created :", link.resultat)) 
  }
  return(list("parameters"=parameters, "lambdaVec"=lambdaVec))

}


