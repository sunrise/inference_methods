##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

## Module 5 : Regression lasso

# For each bootstrap sample, perform a regression of each gene, 
# using the final lambda sequence

##########################################################################
#### FUNCTION ############################################################
##########################################################################

#' Select the adequate variables and return the coeffs of the lasso regression 
#' to explain a gene expression
#'
#' @description
#' performedLasso return the matrix of coeffs (beta) of the Lasso regression
#' on a gene (~ impact of each gene and marker on the gene expression)
#' @param idGene id of the gene for which we want to explain the expression
#' @param variable list of two elements, containing potential explanatory variables
#'          $exp : measurement of gene expression
#'          $snp : measurement of snp
#' @param lambdaVec sequence of lambda to use in the lasso regression
#'
#' @return beta : matrix, beta coefficients of the lasso
#' 
performedLasso <- function(idGene, variable, lambdaVec) {
  # indexes of the gene of interest
  iG = which(colnames(variable$exp)==idGene)
  
  explanatory_vars <- cbind(variable$exp[,-iG],variable$snp)  
  resp_var <- variable$exp[,iG]
  
  # performs the lasso
  out.lars = list("beta" = rep(x = 0, times = ncol(explanatory_vars)))
  out.lars <-glmnet(as.matrix(explanatory_vars), 
                        resp_var, 
                        family = 'gaussian', 
                        lambda = lambdaVec, 
                        standardize = F, 
                        alpha = 1)
  
  ## One vector per value of lambda : ncol of the matrix = length(lambdaVec)
  beta = as.matrix(out.lars$beta)
  rownames(beta) = colnames(explanatory_vars)
  
  return(beta)
}

##########################################################################
#### MAIN ################################################################
##########################################################################
fct_lassoReg <- function(parameters, databoot, lambdaVec, writeRes=F){
  
  listIdGene = parameters$idExp
  names(listIdGene) = parameters$idExp
  
  betaReg = mclapply(databoot, 
                     function(x) lapply(listIdGene, 
                                   function(y) performedLasso(idGene = y, 
                                                              variable = x, 
                                                              lambdaVec = lambdaVec)),
                mc.cores = parameters$nbCores)
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  if(writeRes==T){  
    link.resultat <- paste0(parameters$fileRep,"/temp/betaOfAllRegression",".Rdata")
    save(betaReg, parameters, file = link.resultat)
    
    print(paste("Object created :", link.resultat))
  }
  return(list("parameters"=parameters, "betaReg"=betaReg))

}