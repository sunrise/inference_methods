##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Module 6 : Compile the results of lasso regressions

##########################################################################
#### FUNCTION ############################################################
##########################################################################

#' Number of times the edges of a gene expression are found
#' 
#' @description
#' nbFindEdgePerGene return for each gene expression, the number of times the 
#' edges between this gene and explanatory variables (expression and snp) 
#' are found in the sample, while the lambda vary.
#'
#' @param idGene id of the gene
#' @param varList list of potential explanatory variables (including the gene)
#' @param betaGene beta values for this gene in this bootstrap sample
#'
#' @return nbFindEdgeGeneBoot : numeric, number of times the edges are found
nbFindEdgePerGene <- function(varList, betaGene){
  betaBinary <- matrix(data = 0,
                     ncol = ncol(betaGene),
                     nrow = length(varList),
                     dimnames = list(row = varList))

  betaBinary[rownames(betaGene),][which(abs(betaGene)>0)] <- 1
  
  nbFindEdgeGene = rowSums(betaBinary)
  
  return(nbFindEdgeGene)
}
#------------------------------------------------------------------------#

#' nbFindEdgePerBoot
#' Number of times the edges are found between explanatory variables and genes
#' 
#' @description 
#' For a bootstrap sample, count the number of times the edges from an 
#' explanatory variable (expression or snp) to a gene expression is found

#' @param betaBoot beta coeffs obtained for the bootstrap sample
#' @param idexp,idsnp vectors of identifiants (expression and snp)
#'
#' @return matEdge : matrix, number of times the edges are found between
#' an explanatory variable (row) and a gene (column)
nbFindEdgeOneBoot <-function(betaBoot, idexp, idsnp){
  nbEdge = unlist(lapply(X = betaBoot, 
                         function(x) nbFindEdgePerGene(varList = c(idexp, idsnp), betaGene = x)))
  matEdge = matrix(data = nbEdge, nrow = length(idexp) + length(idsnp), 
                   dimnames = list(row = c(idexp,idsnp), 
                                   col = idexp))
}

##########################################################################
#### MAIN ################################################################
##########################################################################
fct_lassoRes <- function(parameters, betaReg){
  
  # In each bootstrap : number of times the edges are found for each lambda value
  nbFindEdgePerBoot = lapply(X = betaReg, function(x) nbFindEdgeOneBoot(betaBoot = x, 
                                                                     idexp = parameters$idExp, 
                                                                     idsnp = parameters$idSnp))
  
  # combine the bootstraps -> total number of times the edge is found
  nbFindEdge = Reduce(f = `+`, x = nbFindEdgePerBoot)
  
  #------------------------------------------------------------------------#
  
  # to data.frame
  edgeList = as.data.frame(as.table(nbFindEdge))
  colnames(edgeList) = c("Gj","Gi","nbFind")
  
  # removing the non found edges
  edgeListFind = edgeList[-which(edgeList$nbFind == 0),]
  
  # sorting edges : from the more found to the least found 
  edgeListFind = edgeListFind[order(edgeListFind$nbFind,decreasing = TRUE),c("Gj","Gi","nbFind")]
  
  # separation in two sets exp -> exp and snp -> exp
  edgeListExpExp = edgeListFind[which(as.character(edgeListFind$Gj) %in% parameters$idExp),]
  edgeListSnpExp = edgeListFind[which(as.character(edgeListFind$Gj) %in% parameters$idSnp),]
  
  # edgeListExpExp$scoreRank <- rank(edgeListExpExp$nbFind, "min", na.last=T)/(nrow(edgeListExpExp)+1)
  # edgeListSnpExp$scoreRank <- rank(edgeListSnpExp$nbFind, "min", na.last=T)/(nrow(edgeListSnpExp)+1)
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  
  # List of edges expression - expression
  ficOutEdgeExpExp =  paste0(parameters$fileRep, "lasso_edgeListExp2Exp",".tsv")
  write.table(x = edgeListExpExp, file = ficOutEdgeExpExp, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 1/3 :", ficOutEdgeExpExp))
  
  # List of edges snp - expression
  ficOutEdgeSnpExp =  paste0(parameters$fileRep, "lasso_edgeListSnp2Exp",".tsv")
  write.table(x = edgeListSnpExp, file = ficOutEdgeSnpExp, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 2/3 :", ficOutEdgeSnpExp))
  
  # List of all edges 
  ficOutEdgeAll =  paste0(parameters$fileRep, "lasso_edgeListAll",".tsv")
  write.table(x = edgeListFind, file = ficOutEdgeAll, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 3/3 :", ficOutEdgeAll))
  
} 
