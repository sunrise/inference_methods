# Network inference with lasso method, using two types of data : gene expression data and snp data for different genotypes.

Different values of lambda are tested, and a bootstrap of the data is performed.
The lasso is executed at the same time for the snp and expression data.

## Main script : [runLasso.r](./scripts/lasso/runLasso.r)

Parameters to set :

 * **infileExp**: Link to expression file (tab separated, with header)
 * **infileSnp**: Link to snp/haplotype file (tab separated, with header)
 * **GenesAsRows**: format of data, TRUE for genes in rows, FALSE for genes in columns
 * **dataScale**: what data to scale ? 0 none, 1 : only expression data, 2 only haplotype/snp data, 3 : both
 * **nbLambda**: size of the lambda vector
 * **nbCores**: Number of cores used to compute lambdamax (the function is paralellized) - default to detectCores() - 1
 * **outDir**: Directory for outputs (the results will be stored in "outDir/outFileName/out_randomForest")
 * **outFileName**: generic name for output files/directories
 * **nbBootstrap**: number of bootstrap samples
 * **silent**: Functions run silently (TRUE - errors handled if any) or not (FALSE)
 * **writeRes**: Boolean, should the intermediate results also be writen ? Default to FALSE.


[runLasso.r](./scripts/lasso/runLasso.r) call 7 intermediate scripts: 

  * [0-runLasso_fct.r](./scripts/lasso/0-runLasso_fct.r): main function to run the lasso
  * [1-dataConversion.r](./scripts/lasso/1-dataConversion.r): reading and scaling the data
  * [2-generiqueBootstrap.r](./scripts/lasso/2-generiqueBootstrap.r): bootstrap
  * [3-lassoLBDMAX.r](./scripts/lasso/3-lassoLBDMAX.r): selection of lambda max
  * [4-lassoLBDVEC.r](./scripts/lasso/4-lassoLBDVEC.r): creating the lambda vector
  * [5-lassoRegression.r](./scripts/lasso/5-lassoRegression.r): run the different lasso regressions
  * [6-lassoResults.r](./scripts/lasso/6-lassoResults.r) : combine the results


## Required libraries

`glmnet` and `parallel`.

## Produced files are in

    * RESULTS/*outFileName*/out_lasso/
    * .............................../temp/*outFileName*_AllData.Rdata
    * .............................../temp/bootdata_nbBoot_*nbBootstrap*.Rdata
    * .............................../temp/lambdaMax.Rdata
    * .............................../temp/LBDvecteur.Rdata
    * .............................../temp/betaOfAllRegression.Rdata
    * .............................../lasso_edgeListAll.tsv
    * .............................../lasso_edgeListExp2Exp.tsv
    * .............................../lasso_edgeListSnp2Exp.tsv

