#!/usr/bin/Rscript
##########################################################################
#### REQUIREMENTS ########################################################
##########################################################################

# # on server
# .libPaths("~/work/Rlib/")
# setwd(dir = "~/save/inference_sunrise")
# repOut <- "~/work/RESULTS/100simulatedDataSet/" # On server
# nbcorestouse <- 10

# On PC 
repOut <- "RESULTS/"
nbcorestouse <- detectCores()-1


library(glmnet)
library(parallel)

# Calls to functions
source("scripts/lasso/0-runLasso_fct.r")
source("scripts/lasso/1-dataConversion.r")
source("scripts/lasso/2-generiqueBootstrap.r")
source("scripts/lasso/3-lassoLBDMAX.r")
source("scripts/lasso/4-lassoLBDVEC.r")
source("scripts/lasso/5-lassoRegression.r")
source("scripts/lasso/6-lassoResults.r")

##########################################################################
#### EXECUTION #########################################################
##########################################################################

## Script pour executer l'inference de reseau avec lasso en bootstrapant
# et en utilisant différentes valeur de lambda possible

# 6 steps : 
# - reading and scaling the data
# - bootstrap
# - selection of lambda max in each bootstrap
# - creating the common lambda vector to all genes, and bootstrap 
# - run the different lasso regressions, with each lambda value in each bootstrap
# - combine the results of the bootstrap and lambda

#### Run on example dataset :

# runLasso(infileExp="data/exampleData_expression.tsv",
#          infileSnp="data/exampleData_haplotype.tsv", 
#          GenesAsRows = TRUE, 
#          dataScale=1,
#          nbLambda=20)

#### Execution on 100 simulated datasets  
namefilesExp <- list.files(path="data/100simulatedDataSet/artificialDataSet_expression", full.names = T, recursive = T, pattern = "*expression.tsv")
namefileSnp <- "data/100simulatedDataSet/artificialDataSet_haplotype.tsv"

todoExp <- namefilesExp

# # Already done (from exp file names) :
# doneFiles <- list.files(repOut, recursive = T, pattern="lasso_edgeListAll.tsv")

# # Still to do: todoExp
# if(length(doneFiles)>0){
#   doneFiles <- substr(doneFiles, 1, regexpr("/", doneFiles)-1)
#   doneFiles <- paste0("data/100simulatedDataSet/artificialDataSet_expression/", substr(doneFiles,1,2), "_", substr(doneFiles,3,4), "/", doneFiles, ".tsv")
#   todoExp <- namefilesExp[-match(doneFiles, namefilesExp)]
# } else {
#   todoExp <- namefilesExp
# }


# Here parallelization is done accross expression files
mclapply(todoExp, function(x){
  runLasso(x, namefileSnp, GenesAsRows = TRUE, dataScale = 1,
           nbCores = 1, 
           outDir = repOut, 
           outFileName = NULL, 
           nbBootstrap=50, nbLambda = 100)
}, mc.cores = nbcorestouse)




