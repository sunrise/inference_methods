#!/bin/bash
#SBATCH -J RFsunrise
#SBATCH -t 48:00:00
#SBATCH --mem=8G
#SBATCH -c 10
#SBATCH --mail-type=BEGIN,END,FAIL
#Purge any previous modules
module purge

#Load the application
module load system/R-3.6.2

# My command lines I want to run on the cluster
Rscript ~/save/inference_sunrise/scripts/lasso/runLasso.r