##########################################################################
#### DESCRIPTION #########################################################
##########################################################################
# For a dataset and a method of inference, harmonize the scores, names and 
# data structures for meta-analysis
#
# Compute scores
# ------------------------------------------------------------------------
# Take the 2 data files out of methods (Exp2Exp and Snp2Exp), (or only one) 
# Compute the ranks 
# Average the ranks

#
##########################################################################
#### LIBRARY ############################################################
##########################################################################

library(parallel)

##########################################################################
#### FUNCTION ############################################################
##########################################################################

# ensemble de fonction perso pour analyser reseau inferer (recall precision)
source(file = "scripts/metaAnalyse/standardize_functions.r")

# ####################################################################################
# ####################################################################################
# ####### EXECUTION
# 

### On 100 simulated datasets
listdataset <- dir("RESULTS/100simulatedDataSet/", pattern="_expression", full.names = F)

# rfElise
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("rfElise", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="rfElise_edgeListExp2Exp", "snp"="rfElise_edgeListSnp2Exp"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)


# PEMRF
mclapply(listdataset, function(currentdataset)  
fct_standardize_scores("PEMRF", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                       patternfiles = list("exp"="rank_expression_pemrf", "snp"="rank_marker_pemrf"),
                       nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                       colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                       repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
, mc.cores = detectCores()-1)


# randomForest
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("randomForest", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="rf_edgeListExp2Exp", "snp"="rf_edgeListSnp2Exp"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)


# lasso
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("lasso", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="lasso_edgeListExp2Exp", "snp"="lasso_edgeListSnp2Exp"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)

# bayesianNetwork
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("bayesianNetwork", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = "haplotype_gene_expression_edges.txt",
                         nbEdgefiles = 1, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep=" ",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)


# bayesianNetwork (new method = geneBayesNet)
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("geneBayesNet", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="gbn_genegene.txt", "snp"="gbn_markertogeneonly.txt"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep=" ",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)

# OLS
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("OLS", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="edgeList_exp_withKinship.csv", "snp"="edgeList_snp_withKinship.csv"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=T,
                         colNameFic=c("Gj","Gi","pVal"), sep=" ",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)

# space
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("spaceMB", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="spaceMB_edgeListExp2Exp", "snp"="spaceMB_edgeListSnp2Exp"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)

# genenet
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("genenetstaticlambda", currentdataset, paste0("RESULTS/100simulatedDataSet/", currentdataset, "/"),
                         patternfiles = list("exp"="genenetlambdastatic_edgeListExp2Exp", "snp"="genenetlambdastatic_edgeListSnp2Exp"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep="\t",
                         repOut=paste0("RESULTS/100simulatedDataSet/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)




### 
# Measured data
listdataset <- "15EX05_expression_noNA"
# bayesianNetwork (new method = geneBayesNet)
mclapply(listdataset, function(currentdataset)  
  fct_standardize_scores("geneBayesNet", currentdataset, paste0("RESULTS/", currentdataset, "/"),
                         patternfiles = list("exp"="geneBayesNet_genegeneonly.txt", "snp"="geneBayesNet_markertogeneonly.txt"),
                         nbEdgefiles = 2, possibleID=c("E", "M", "G") , pVal=F,
                         colNameFic=c("Gj","Gi","nbFind"), sep=" ",
                         repOut=paste0("RESULTS/", currentdataset, "/in_metaanalysis/"))
  , mc.cores = detectCores()-1)
