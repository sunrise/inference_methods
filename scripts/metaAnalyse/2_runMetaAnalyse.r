##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Script to run the meta analysis on the results from the different methods 
# used for network inference

##########################################################################
#### REQUIREMENTS ########################################################
##########################################################################

library(parallel)
source(file = "scripts/metaAnalyse/metaAnalyse_functions.r")

##########################################################################
#### EXECUTION ##########################################################
##########################################################################

# 100 simulated datasets  
listDir <- list.dirs("RESULTS/100simulatedDataSet", full.names = T, recursive = F)

# # Already done (from exp file names) : 
doneFiles <- list.files("RESULTS/100simulatedDataSet", recursive = T, pattern="metaAnalyse_edgeList.tsv", full.names = F)

# Still to do: todoExp
if(length(doneFiles)>0){
  doneFiles <- substr(doneFiles, 1, regexpr("/", doneFiles)-1)
  doneFiles <- paste0("RESULTS/100simulatedDataSet/", doneFiles)
  todoDir <- listDir[-match(doneFiles, listDir)]
} else {
  todoDir <- listDir
}

t <- Sys.time()
mclapply(todoDir, function(x) fct_metaanalysis(methods = c("lasso","randomForest","OLS","PEMRF","bayesianNetwork", "findr"), 
                                               repDataset=x))
Sys.time() - t

t <- Sys.time()
mclapply(todoDir, function(x) fct_metaanalysis(methods = c("lasso","randomForest","OLS","PEMRF","geneBayesNet", "findr"), 
                                               repDataset=x))
Sys.time() - t

# Measured dataset

fct_metaanalysis(methods = c("lasso","randomForest","OLS","PEMRF","bayesianNetwork", "findr"), 
                 repDataset="RESULTS/15EX05_expression_noNA")



fct_metaanalysis(methods = c("lasso","randomForest","OLS","PEMRF","geneBayesNet", "findr"), 
                 repDataset="RESULTS/15EX05_expression_noNA")



