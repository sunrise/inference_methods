# Meta-analysis on network inference methods

Script to run the meta analysis on the results from the different methods 
used for network inference
- lasso
- random forest
- PE-MRF
- OLS
- bayesian network
- findr


## Prepare the data [1_standarizeScore4metaAnalyse.r](./scripts/metaAnalyse/1_standarizeScore4metaAnalyse.r)

The main function standardize the data to be used in the metaanalysis (colnames, scores), for the different methods.

This script calls [standardize_functions.r](./scripts/metaAnalyse/standardize_functions.r).

### Parameters
 * **method**: name of the method
 * **nameDataset**: name of the dataset
 * **repIn**: directory to look in for the entry dataset (results of the method)
 * **patternfiles**: pattern of the file of the method results
 * **nbEdgefiles**: how much files of results (2 for separate results between Snp->Exp and Exp->Snp, 1 otherwise)
 * **possibleID**: possible patterns for the genes names
 * **pVal**: boolean - is the result of the method a p-value ?
 * **colNameFic** names of the columns in the result files
 * **sep**: separator of the method results files
 * **repOut**: out directory to write the standardize files
          
### Output files

Produced files will be:

    * RESULTS/*repOut*/in_metaanalysis/*method*_edgeList.tsv
    
## Main script : [2_runMetaAnalyse.r](./scripts/metaAnalyse/2_runMetaAnalyse.r)

2_runMetaAnalyse.R calls [metaAnalyse_functions.r](./scripts/metaAnalyse/metaAnalyse_functions.r), where all the functions used are defined. 

4 steps :

* opening the different results
* computing/harmonizing scores from each method
* run the meta analysis
* write the results in RESULTS/datasetname/out_metaanalysis/

### Parameters
* **methods**: Vector of methods on which to perform the metaanalysis
* **repDataset**: Directory of the results for the dataset. This directory must include 
      a sub-directory in_metaanalysis. A out_metaanalysis directory will be created.
* **repOut**: Directory to write the metaanalysis results in. If NULL, will be repDataset/out_metaanalysis.
          
### Output files

Produced files will be:

    * RESULTS/*repOut*/out_metaanalysis/metaAnalyse_edgeList.tsv
    
## Required libraries

1_standarizeScore4metaAnalyse.R needs the `parallel` library.


