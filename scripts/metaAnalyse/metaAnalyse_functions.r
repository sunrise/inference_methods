
##########################################################################

#' metaAnalyse1edge
#' performed metaAnalyse for 1 edge on the score obtain for different
#' infered methods. 
#'
#' @param data inference score obtain for different inference currentmethod
#' @param currentmethod methods used for metaAnalyse
#'
#' @return edge score with metaAnalyse
metaAnalyse1edge <- function (dataedge, methods = c("lasso","randomForest","OLS","PEMRF","bayesianNetwork")) {
  nameScore <- paste0(methods, "Score")
  
  dataedge[is.na(dataedge)] <- 0
  metaAnalyseScore <- 1-exp(
    sum(
      log(1-as.numeric(dataedge[nameScore])),
      na.rm=TRUE)
  )
  
  return(metaAnalyseScore)
}


##########################################################################

#' metaAnalyseAllEdge
#' run the metaAnalyse on the results obtain for differenct selected
#' inference methods.
#'
#' call function : metaAnalyse1edge()
#'
#' @param info list containing different information:
#'         $currentmethod name of select inference currentmethod for metanalyse
#'         $dataSetName name of the dataset
#'         $repIn (not used)
#'         $repOut directory containing result of metaAnalysis
#'         $edgesScoreAllMethod for each edge score in the different currentmethod
#'
#'
#' @return edgesScoreAllMethod
metaAnalyseAllEdge <- function(info)
{
  # performed metaAnalyse on each edge
  info$edgesScoreAllMethod$metaAnalyseScore <- apply(info$edgesScoreAllMethod, 
                                                     MARGIN = 1, 
                                                     function(x) metaAnalyse1edge(dataedge = x, methods = info$methods))
  
  # order results by metaAnalyse score
  edgesScoreAllMethod <- info$edgesScoreAllMethod[order(info$edgesScoreAllMethod$metaAnalyseScore,
                                                             decreasing = TRUE),]
  
  return(edgesScoreAllMethod)
}

##########################################################################

#' loadMethodEdgeList
#' load edges find by a specific inference currentmethod and the score
#' associated to this currentmethod
#'
#' @param currentethod name of the inference method
#' @param ficInList list of all edges files
#'
#' @return dataframe of edge find by the currentmethod
#'    3 colums "Gj" regulator gene
#'             "Gi" regulated gene
#'             "score" score associated to the edge
loadMethodEdgeList <- function(currentmethod, ficInList) {
  ficMet <- ficInList[grep(pattern = currentmethod, x = ficInList, fixed = T)]
  scoreName <- paste0(currentmethod,"Score")
  
  if (length(ficMet) == 1) {
    edgeMet <- read.delim(file = ficMet,
                          header = TRUE,
                          sep = "\t",
                          col.names = c("Gj","Gi", scoreName), stringsAsFactors = F)
  } else {
    print(paste0("No result for the currentmethod ", currentmethod, " on the dataset"))
    edgeMet<-data.frame(Gj = NA, Gi = NA, score = NA)
    colnames(edgeMet) <- c("Gj","Gi",scoreName)
  }
  
  return(edgeMet)
}

##########################################################################

#' openEdgeFiles
#' Open file containing edges obtain for a selection of currentmethod
#' 
#' call function : loadMethodEdgeList()
#'
#' @param info list containing different information:
#'         $currentmethod
#'         $dataSetName
#'         $repIn
#'
#' @return $edgesScoreAllMethod
openEdgeFiles <- function(info)
{
  # vector of files containing edges from inference methods, in repIn
  ficInList <- list.files(path = info$repIn,
                          pattern = "edgeList",
                          recursive = TRUE,
                          full.names = T)
  
  # open files for each currentmethod, NA if not found
  edgesPerMethod <- lapply(info$methods, 
                           function(method) 
                             loadMethodEdgeList(currentmethod = method, ficInList = ficInList))
  
  # unique table
  edgesScoreAllMethod <- Reduce(f = function(x,y) merge(x = x, y = y, by = c("Gj","Gi"), all = TRUE), x = edgesPerMethod)
  
  info$edgesScoreAllMethod <- edgesScoreAllMethod
  
  return(info)
}

###############################################################
# MAIN FUNCTION

#' fct_metaanalysis performs the metaanalysis on one dataset, for the set of methods previded.
#'
#' @param methods Vector of methods on which to perform the metaanalysis
#' @param repDataset Directory of the results for the dataset. This repertory must include a sub-directory 
#' in_metaanalysis. A out_metaanalysis directory will be created.
#' @param repOut Directory to write the metaanalysis results in. If NULL, will be repDataset/out_metaanalysis.
#'
#' @return Character, the name (and location) of the file created. 
#' The function write the results of the metaanalysis on a tsv file, printed on screen.
#' @export
#'
#' @examples
#' fct_metaanalysis(methods=c("lasso", "randomForest), repDataset="RESULTS/exampleData_expression/")
fct_metaanalysis <- function(methods = c("lasso","randomForest","OLS","PEMRF","bayesianNetwork"), 
                             repDataset, repOut=NULL){
  datasetName <- list.dirs(repDataset, recursive = F, full.names = F)
  names(datasetName) <- datasetName
  
  lastchar <- substr(repDataset, nchar(repDataset), nchar(repDataset))
  if(lastchar != "/"){
    repDataset <- paste0(repDataset, "/")
  }
  
  repIn <- paste0(repDataset, "in_metaanalysis/")
  if(is.null(repOut)){
    repOut <- paste0(repDataset, "out_metaanalysis/")
  } else {
    lastchar <- substr(repOut, nchar(repOut), nchar(repOut))
    if(lastchar != "/"){
      repOut <- paste0(repOut, "/")
    }
  }
  
  # creating the folders if needed
  if(dir.exists(repOut)==F){
    dir.create(repOut, recursive = T)
  }
  
  
  ##########################################################################
  #### MAIN ################################################################
  ##########################################################################
  
  # store information in a list
  parameters <- list()
  parameters$methods <- methods
  parameters$repIn <- repIn
  
  # 1 - ouverture des données
  dataedges <- openEdgeFiles(parameters)
  
  # 2 - metaAnalyse
  res <- metaAnalyseAllEdge(dataedges)
  
  # 3 - save the metaanlyse result
  ficOut <- paste0(repOut, "metaAnalyse_edgeList.tsv")
  write.table(x = res, file = ficOut,  sep = "\t", eol = "\n", row.names = FALSE, col.names = TRUE, quote = FALSE)
  
  print(ficOut)
  return(ficOut)
}



