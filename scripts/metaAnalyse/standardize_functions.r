#------------------------------------------------------------------------#

#' separteEdgeByType
#' 
#' Si une liste d'arrètes contient à la fois les arrètes snp -> exp
#' et exp -> exp, separe en 2 liste distinctes, en fonction des
#' noms des noeuds de depart, par defaut E une expression et
#' M un snp (possible de modifier)
#'
#' @param edgeList data frame des arètes contient les colonnes
#'    $Gj : le gène régulateur d'une arète
#'    $Gi : gène régulé d'une arète
#' @param idExp symbole indiquant que le noeud est une expression, defaut "E"
#' @param idSnp symbole indiquant que le noeud est un snp, defaut "M"
#'
#' @return
#' @export
separateEdgeByType <- function(edgeList, idExp = "E", idSnp = "M")
{
  edgeListSep <- list("snp" = edgeList[grep(pattern = idSnp, x = edgeList$Gj),],
                      "exp" = edgeList[grep(pattern = idExp, x = edgeList$Gj),])
  
  return(edgeListSep)
}

#------------------------------------------------------------------------#

#' loadEdgeList
#' Ouvre les fichiers des (deux) listes d'arrètes
#' si une seule liste appelle separateByEdgeType()
#'
#' @param directory dossier contenant les fichiers de liste d'arètes
#' @param nbList nombre de liste d'arètes par defaut 2 (exp->exp + snp->exp)
#' @param fileExp2Exp fichier contenant la liste des arètes exp -> exp
#' @param fileSnp2Exp fichier contenant las liste des arètes snp -> exp
#' @param fileEdge fichier contenant ttes les arètes si 1 seule liste
#' @param sep separateur utilisé entre les colonnes des fichiers
#' @param idExp 
#' @param idSnp 
#'
#' @return liste composé de $exp data frame des arètes exp -> exp
#'    et $snp data frame des arètes snp -> exp
#' @export
loadEdgeList <- function(directory, nbList = 2, 
                         fileExp2Exp = "", 
                         fileSnp2Exp = "", 
                         fileEdge = "", idExp = "E", idSnp = "M", sep = "\t",
                         colNameFic) {
  # open infered network edge list(s)
  if(nbList == 2) {
    infNetwork <- list ("exp" = read.delim(file = paste0(directory,fileExp2Exp), 
                                           header = TRUE, sep = sep,
                                           col.names = colNameFic),
                        "snp" = read.delim(file = paste0(directory,fileSnp2Exp),
                                           header = TRUE, sep = sep,
                                           col.names = colNameFic))
  } else if (nbList == 1) {
    infNetworkList <- read.delim(file = paste0(directory,fileEdge),
                                 header = FALSE, sep = sep,
                                 col.names = colNameFic)
    infNetwork <- separateEdgeByType(edgeList = infNetworkList, idExp = idExp, idSnp = idSnp)
  } else {
    print ("Erreur : 1 ou 2 liste d'arrètes seulement")
  }
  
  return(infNetwork)
}

#-------------------------------------------------------------------------#

#' normalizeScore transform the score into a rank score (inv(rank)/nrow)
#'
#' @param infNetwork the infered network : 3 columns Gj, Gi and nbFind or pVal
#' @param pVal Boolean, default to FALSE. Are the given scores pValues ? Otherwise that's 
#' the nmber of times an edge is found. 
#' @possibleId possible pattern in SNP/Genes names in the columns Gj and Gj. 
#' They will be replaced by "G". Default to c("E","M","G")
#'
#' @return
#' @export
#'
#' @examples
normalizeScore <- function(infNetwork, pVal = FALSE, possibleID = c("E","M","G")){
  if (!pVal) {
    # cas nbre de fois arrète retrouvée (separé suivant type d'arrètes)
    infNetwork$scoreRank <- infNetwork$nbFind
  } else {
    # cas des pVal : (1- pVal.adjust) # prob pour les trop faible p.Val
    infNetwork$scoreRank <- 1 - as.numeric(infNetwork$pVal)
  }
  
  # ordonne les arrètes
  orderInfNetwork <- infNetwork[order(infNetwork$scoreRank,
                                      decreasing = TRUE),]
  
  # modifie les scores des listes pour pouvoir les combiner
  if(nrow(orderInfNetwork)>0)
    orderInfNetwork$scoreRank <- (nrow(orderInfNetwork):1)/nrow(orderInfNetwork)
  
  # modifie le nom des arètes pour pouvoir les combiner
  t <- paste("(", paste(possibleID, collapse = ")|("), ")", sep = "")
  orderInfNetwork$Gj <- sub(pattern = t, replacement = "G", x = orderInfNetwork$Gj, perl = TRUE)
  orderInfNetwork$Gi <- sub(pattern = t, replacement = "G", x = orderInfNetwork$Gi, perl = TRUE)
  
  return(orderInfNetwork)
}



##########################################################################
#### MAIN ################################################################
##########################################################################
fct_standardize_scores <- function(method, nameDataset, repIn, patternfiles, 
                                   nbEdgefiles = 2, possibleID, pVal, colNameFic, sep, repOut){
  
  print(repIn)
#  if(!(method %in% c("PEMRF", "OLS", "lasso", "randomForest", "bayesianNetwork", "rfElise")))
#    stop("methode non prise en compte", call. = T)
  #------------------------------------------------------------------------#
  # FILES TO READ                                                          #
  #------------------------------------------------------------------------#
  
  if(nbEdgefiles==2){
    ficInList <- list("exp" = list.files(path = repIn,
                                         pattern = patternfiles$exp,
                                         recursive = TRUE),
                      "snp" = list.files(path = repIn,
                                         pattern = patternfiles$snp,
                                         recursive = TRUE))
  } else {
    ficInList <- list.files(path = repIn, pattern=patternfiles, recursive=T)
  }
  
  #------------------------------------------------------------------------#
  # READING DATA
  #------------------------------------------------------------------------#
  
  if (nbEdgefiles == 2){
    fileExist <- file.exists(paste0(repIn,ficInList))
  } else if (nbEdgefiles == 1){
    fileExist <- file.exists(paste0(repIn,ficInList))
  }
  fileExist <- identical(fileExist, rep(T, nbEdgefiles))
  print(fileExist)
  
  if(!fileExist){
    warning("no results datafile", call. = T, immediate. = T)
    return(NULL)
  } else {
    if(nbEdgefiles == 2){
      infNetwork <- loadEdgeList(directory = repIn,
                                 fileExp2Exp = ficInList$exp,
                                 fileSnp2Exp = ficInList$snp,
                                 nbList = 2,
                                 sep = sep,
                                 colNameFic=colNameFic)
    } else if(nbEdgefiles == 1){
      infNetwork <- loadEdgeList(directory = repIn,
                                 fileEdge = ficInList,
                                 idExp = "E",
                                 idSnp = "M",
                                 nbList = 1,
                                 sep = sep,
                                 colNameFic=colNameFic)
    }
  }
  #------------------------------------------------------------------------#
  # STANDARIZE SCORE
  #------------------------------------------------------------------------#
  
  # harmonize scores to merge lists
  normInfNetwork <- lapply(infNetwork, FUN = function(x) 
    normalizeScore(infNetwork = x, 
                   pVal = pVal, 
                   possibleID = possibleID))
  
  # merge the two lists
  mergeInfNet <- merge(x = normInfNetwork$exp,
                       y = normInfNetwork$snp,
                       by = c("Gj","Gi"),
                       suffixes = list(x = ".exp", y = ".snp"),
                       all = TRUE)
  
  mergeInfNet[is.na(mergeInfNet)] <- 0
  
  # compute the score for each edge : (scoreSNP + scoreExp)/2
  mergeInfNet$score <- (mergeInfNet$scoreRank.exp + mergeInfNet$scoreRank.snp)/2
  
  #------------------------------------------------------------------------#
  # SAVE          
  #------------------------------------------------------------------------#
  
  # save
  infNet2save <- mergeInfNet[,c("Gj","Gi","score")]
  
  # create directory
  if(!dir.exists(repOut))
  {
    dir.create(repOut, recursive = T)
  }
  
  # save
  write.table(x = infNet2save, file = paste0(repOut, method, "_edgeList.tsv"), 
              sep="\t", col.names = TRUE, row.names = FALSE, eol = "\n")
  print(paste0(repOut, method, "_edgeList.tsv"))
  
}
