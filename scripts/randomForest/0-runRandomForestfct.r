#' removeExt
#'
#' Remove any common extension from a vector of file names
#' by Gordon Smyth, 19 July 2002.  Last modified 19 Jan 2005.
#' 
#' @param x file name with extention and path
#'
#' @return file name without extension and paths
removeExt <- function(x) {
  x <- as.character(x)
  x <- basename(x)
  n <- length(x)
  ext <- sub("(.*)\\.(.*)$","\\2",x)
  if(all(ext[1] == ext))
    return(sub("(.*)\\.(.*)$","\\1",x))
  else
    return(x)
}

##########################################################################
#### MAIN FUNCTION #######################################################
##########################################################################

#' Title
#'
#' @param infileExp Link to expression file (tab separated, with header)
#' @param infileSnp Link to snp/haplotype file (tab separated, with header)
#' @param GeneAsRows Are genes in rows (TRUE, default) or not (FALSE)
#' @param dataScale What data to scale ?
#'  1 : only expression data (default)
#'  2 : only haplotype/snp data
#'  3 : both : expresssion + haplotype/snp
#' @param nbTree Number of trees to use in randomForest function
#' @param nbCores Number of cores to use for parallelization. 
#' If NULL (default), use parallel::detectCores() - 1.
#' If 1, the function won't be parallelized. 
#' @param outDir Directory for outputs (the results will be stored in "outDir/outFileName/out_randomForest")
#' @param outFileName Directory for outputs (lats part, usually depending on the name of input files).
#' If NULL (default), it will be the name of expression data file, whithout extension.
#' @param writeRes Boolean, should the intermediate results also be writen ?
#'
#' @return NULL
#' @export
#'
#' @examples
#' runrandomForest("data/exampleData_expression.tsv",
#'                 "data/exampleData_haplotype.tsv", 
#'                 GenesAsRows = TRUE, 
#'                 dataScale=1,
#'                 nbTree=100)
runrandomForest <- function(infileExp, infileSnp, GenesAsRows = c(TRUE, FALSE), dataScale = c(1,2,3), 
                            nbTree = 100, nbCores = NULL, outDir = "RESULTS/", outFileName = NULL, 
                            writeRes = F, printinterm=F) {
  
  if(file.exists(infileExp)==FALSE){
    stop("Expression file doesn't exists")
  }
  if(file.exists(infileSnp)==FALSE){
    stop("Expression file doesn't exists")
  }
  
  lastchar <- substr(outDir, nchar(outDir), nchar(outDir))
  if(lastchar != "/"){
    outDir <- paste0(outDir, "/")
  }
  
  if(is.null(outFileName)){
    outFileName <- paste(removeExt(infileExp), collapse="")
  } else {
    lastchar <- substr(outFileName, nchar(outFileName), nchar(outFileName))
    if(lastchar != "/"){
      outFileName <- paste0(outFileName, "/")
    }   
  }
  
  # Location of output and temporary files
  fileRep <- paste0(outDir, outFileName, "/out_randomForest/")
  
  if(is.null(nbCores)) nbCores <- detectCores() - 1
  
  ##########################################################################
  #### MAIN ################################################################
  ##########################################################################
  
  parameters <<- list("infileExp" = infileExp,
                      "infileSnp" = infileSnp,
                      "outFileName" = outFileName,
                      "dataScale" = dataScale,
                      "GenesAsRows" = GenesAsRows,
                      "nbTree" = nbTree,
                      "fileRep" = fileRep,
                      "nbCores" = nbCores)
  
  # creating the repertory if needed
  if(!dir.exists(fileRep)){
    dir.create(fileRep, recursive = T)
  }
  # creating the repertory if needed
  if(writeRes==T){
    fileRepTemp <- paste0(fileRep, "temp/")
    if(!dir.exists(fileRepTemp)){
      dir.create(fileRepTemp, recursive = T)
    }
  }
  
  if(printinterm) print("# 1/3 - loading/conversion of the data")
  if(printinterm) tps <- Sys.time()
  step1 <- fct_randomForestInit(parameters, writeRes=writeRes)
  if(printinterm) print(Sys.time()-tps)
  
  if(printinterm) print("# 2/3 - Regression by random forest for each gene")
  ## Long step !!
  if(printinterm) tps <- Sys.time()
  step2 <- fct_randomForestReg(step1$parameters, step1$dataExp, step1$dataSnp, writeRes=writeRes)
  if(printinterm) print(Sys.time()-tps)
  
  if(printinterm) print("# 3/3 - Combine the results")
  if(printinterm) tps <- Sys.time()
  step3 <- fct_randomForestRes(step2$parameters, step2$weightAdj)
  if(printinterm) print(Sys.time()-tps)
  
}


