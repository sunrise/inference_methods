##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Module 1 :
# Conversion of gene expression data -> .Rdata
# Creating the genes names if missing
# Verify the data orientation, change it if needed

##########################################################################
#### MAIN ################################################################
##########################################################################
fct_randomForestInit <- function(parameters, writeRes=F){
  ## Read the files in args
  dataExp <- read.table(file = parameters$infileExp, header = TRUE, sep = "\t", dec = ".", stringsAsFactors = F)
  dataSnp <- read.table(file = parameters$infileSnp, header = TRUE, sep = "\t", dec = ".", stringsAsFactors = F)
  
  # If genes in rows
  if (parameters$GenesAsRows){
    dataExp <- t(dataExp)
    dataSnp <- t(dataSnp)
  }
  
  # Creating gene names if not existing
  if(is.null(colnames(dataExp)))
  {
    colnames(dataExp)<-paste("E",c(1:ncol(dataExp)),sep=(""))
    colnames(dataSnp)<-paste("M",c(1:ncol(dataSnp)),sep=(""))
  }
  
  # Creating genotype names if not existing
  if(is.null(rownames(dataExp)))
  {
    rownames(dataExp)<-paste("IND",c(1:nrow(dataExp)),sep=(""))
    rownames(dataSnp)<-paste("IND",c(1:nrow(dataSnp)),sep=(""))
  }
  
  # If common names for expression and SNP
  if(length(intersect(x = colnames(dataExp), y = colnames(dataSnp)))>0){
    colnames(dataSnp)<- paste0(colnames(dataSnp),".SNP")
  }
  
  parameters$idExp = colnames(dataExp)
  parameters$idSnp = colnames(dataSnp)
  parameters$idGenotype = rownames(dataSnp)
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  if(writeRes==T){
    dirTemp <- paste0(parameters$fileRep, "temp/")
    link.resultat = paste0(dirTemp, parameters$outFileName, "_AllData.Rdata")
    save(parameters, dataExp, dataSnp, file = link.resultat)
    print(paste("Object created :", link.resultat))
    
  }
  # creating the repertory if needed
  return(list("parameters"=parameters, 
              "dataExp"=dataExp, 
              "dataSnp"=dataSnp))
  
}
