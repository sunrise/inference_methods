##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

## Module 2 : Regression Random Forest

# For each gene, performs a random forest

##########################################################################
#### FUNCTION ############################################################
##########################################################################

#' performedRandomForest
#'
#' @description 
#' Performs a randomForest on the expression of a gene, 
#' using the expression data (minus the gene of interest) 
#' and the snp data as explanatory variables.
#' @param idGene Id of the gene on which to perform the regression
#' @param dataExp Measurement of the genes expression
#' @param dataSnp Measurement of snp
#' @param nbTree Number of tree random Forest (default = 1000)
#'
#' @return weightAdj Weights of the regression
performRandomForest <- function(idGene, dataExp, dataSnp, nbTree = 1000){
  # indexes of the gene in the variables set
  iG = which(colnames(dataExp)==idGene)
  
  # rf for the gene
  # random forest pour le gène (varExplicative :  expression des autres gènes + ttes les mesures de SNP)
  rf <- randomForest(x = cbind(dataExp[,-iG],dataSnp), y = dataExp[,iG], ntree = nbTree, importance = T, na.action = na.omit)
  
  # Weight or score matrix for edges  
  rfweightAdj <- rep(0, times = ncol(dataExp)+ncol(dataSnp))
  rfweightAdj[-iG] <- rf$importance[,"%IncMSE"]/rf$importanceSD
  
  return(rfweightAdj)
}

##########################################################################
#### MAIN ################################################################
##########################################################################

fct_randomForestReg <- function(parameters, dataExp, dataSnp, writeRes){
  listIdGene = parameters$idExp
  names(listIdGene) = parameters$idExp
  
  weightAdj = mclapply(X = listIdGene,
                       function(x) performRandomForest(idGene = x,
                                                       dataExp = dataExp,
                                                       dataSnp = dataSnp,
                                                       nbTree = parameters$nbTree),
                       mc.cores = parameters$nbCores)

##########################################################################
#### SAVE ################################################################
##########################################################################
  if(writeRes==T){
    link.resultat <- paste0(fileRep, "temp/regressionRF_allGene.Rdata")
    save(weightAdj, parameters, file = link.resultat)
    print(paste("Object created :", link.resultat))
  }
  return(list("parameters"=parameters, "weightAdj"=weightAdj))
}
