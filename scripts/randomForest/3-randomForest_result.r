##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

## Module 3 : Combine the results of randomForests

##########################################################################
#### MAIN ################################################################
##########################################################################

fct_randomForestRes <- function(parameters, weightAdj, printinterm=T){
  # weights of rf regressions into matrix
  # columns : explained genes
  # rows : explanatory variables (expression and snp)
  weightAdj.mat <- as.matrix(as.data.frame(weightAdj, row.names = c(parameters$idExp, parameters$idSnp)))
  
  # Sort the values of the regression : highest = highest score/rank
  weightSorted <- sort(weightAdj.mat, decreasing = T, index.return = T, na.last = TRUE)
  weightSorted <- data.frame(do.call("cbind", weightSorted), stringsAsFactors = F)
  
  # nb found edges (no NA and no 0 and no negative)
  nbEdge <-nrow(weightSorted[!is.na(weightSorted$x) & weightSorted$x>0,]) 
  
  # creating the matrix of rank/score of each edge (best edge = highest)
  rankAdj = matrix(data = c(NA),
                   nrow = length(c(parameters$idExp,parameters$idSnp)),
                   ncol = length(parameters$idExp),
                   dimnames = list("row" = c(parameters$idExp,parameters$idSnp),
                                   "col" = parameters$idExp))
  
  idnoNAPOS <- weightSorted[weightSorted$x>0 & !is.na(weightSorted$x),]$ix
  rankAdj[idnoNAPOS] <- c(nbEdge:1)
  
  #------------------------------------------------------------------------#
  
  # # Ajout Elise
  # ELISEedgeListFind <- data.frame(do.call("cbind", weightAdj), stringsAsFactors = F)
  # ELISEedgeListFind <- cbind("Gj"=c(parameters$idExp, parameters$idSnp), ELISEedgeListFind, stringsAsFactors=F)
  # ELISEedgeListFind <- reshape(ELISEedgeListFind, varying = list(2:ncol(ELISEedgeListFind)), 
  #                              timevar="Gi", idvar="Gj", times=colnames(ELISEedgeListFind)[-1], direction="long")
  # colnames(ELISEedgeListFind)[3] <- "weights"
  # ELISEedgeListFind <- ELISEedgeListFind[is.na(ELISEedgeListFind$weights)==F & ELISEedgeListFind$weights>0,]
  # 
  # # separation in two sets exp -> exp and snp -> exp
  # ELISEedgeListExpExp = ELISEedgeListFind[edgeListFind$Gj %in% parameters$idExp,]
  # ELISEedgeListSnpExp = ELISEedgeListFind[edgeListFind$Gj %in% parameters$idSnp,]
  
  
  #-------------------------------------------------------------------------
  # to data.frame
  edgeList = as.data.frame(as.table(rankAdj))
  colnames(edgeList) = c("Gj","Gi","nbFind")
  
  # removing the non found edges
  edgeListFind = edgeList[is.na(edgeList$nbFind)==F,]
  
  # sorting edges : from the more found to the least found 
  edgeListFind = edgeListFind[order(edgeListFind$nbFind,decreasing = TRUE),c("Gj","Gi","nbFind")]
  
  # separation in two sets exp -> exp and snp -> exp
  edgeListExpExp = edgeListFind[as.character(edgeListFind$Gj) %in% parameters$idExp,]
  edgeListSnpExp = edgeListFind[as.character(edgeListFind$Gj) %in% parameters$idSnp,]
  
  ##########################################################################
  #### SAVE ################################################################
  ##########################################################################
  
  # List of edges expression - expression
  ficOutEdgeExpExp =  paste(parameters$fileRep,"rf_edgeListExp2Exp.tsv", sep = "")
  write.table(x = edgeListExpExp, file = ficOutEdgeExpExp, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 1/3 :", ficOutEdgeExpExp))
  
  # List of edges snp - expression
  ficOutEdgeSnpExp =  paste(parameters$fileRep,"rf_edgeListSnp2Exp.tsv", sep = "")
  write.table(x = edgeListSnpExp, file = ficOutEdgeSnpExp, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 2/3 :", ficOutEdgeSnpExp))
  
  # List of all edges 
  ficOutEdgeAll =  paste(parameters$fileRep,"rf_edgeListAll.tsv", sep = "")
  write.table(x = edgeListFind, file = ficOutEdgeAll, sep = "\t", row.names = FALSE, col.names = TRUE)
  print(paste("Object created 3/3 :", ficOutEdgeAll))
}
  
