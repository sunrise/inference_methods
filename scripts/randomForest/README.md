# Network inference with random forest method, using two types of data : gene expression data and snp data for different genotypes.

Different values of lambda are tested, and a bootstrap of the data is performed.
The randomForest package method is executed at the same time for the snp and expression data.

## Main script: [runRandomForest.r](./scripts/randomForest/runRandomForest.r)

Parameters to set:
 * **infileExp**: link to the expression file
 * **infileSnp**: link to the haplotype file
 * **GenesAsRows**: format of data, TRUE for genes in rows, FALSE for genes in columns
 * **dataScale**: what data to scale ? 0 none, 1 : only expression data, 2 only haplotype/snp data, 3 : both
 * **nbTree**: Number of trees in the random forest
 * **nbCores**: Number of cores used to compute the randomForests - default to detectCores() - 1
 * **outDir**: Directory for outputs (the results will be stored in "outDir/outFileName/out_randomForest")
 * **outFileName**: generic name for output files/directories (if NULL, name of the expression data file)
 * **writeRes**: Boolean, should the intermediate results also be writen ?


[runRandomForest.r](./scripts/randomForest/runRandomForest.r) call 4 intermediate scripts : 
  * [0-randomForest_init.r](./scripts/randomForest/0-runRandomForestfct.r) : main function used, calling the 3 intermediate steps 
  * [1-randomForest_init.r](./scripts/randomForest/1-randomForest_init.r) : reading the data
  * [2-randomForest_regression.r](./scripts/randomForest/2-randomForest_regression.r) : run the randomForests
  * [3-randomForest_results.r](./scripts/randomForest/3-randomForest_results.r) : combine the results


## Required libraries

`parallel` and `randomForest`

## Produced files are in

    * RESULTS/*outFileName*/out_randomForest/
    * ....................................../temp/*outFileName*_AllData.Rdata
    * ....................................../temp/regressionRF_allGene.Rdata
    * ....................................../rf_edgeListAll.tsv
    * ....................................../rf_edgeListExp2Exp.tsv
    * ....................................../rf_edgeListSnp2Exp.tsv

