#!/usr/bin/Rscript
##########################################################################
#### DESCRIPTION #########################################################
##########################################################################

# Inference of regulation network with random forest on expression
# and snp data measured for a group of genes on different genotype

# 3 steps for the inference :
#    - scale and data conversion
#    - random forest regression
#    - creation of edge liste

# library : randomForest

# input files :
#    - expression des gènes dans les différents génotypes (argument 1)
#    - mesure snp-haplotype de chaque gène dans les différents génotypes (argument 2)
# les fichiers d'entrées peuvent etre soit avec les gènes en colonne
# soit avec les genotypes en colonne choisir avec l'option : format
# les fichiers d'entrée sont spécifié en argument du script

# Plusieurs paramètres a regler dans le script :
# - nom generique des fichiers de sortie, ex : expe15EX05
# - format des données (genes en ligne ou en colonne)
# - normalisation des données oui/non
# - nbre d'arbre dans le random forest

##########################################################################
#### REQUIREMENTS ########################################################
##########################################################################




# on server
.libPaths("~/work/Rlib/")
setwd(dir = "~/save/inference_sunrise")
repOut <- "~/work/RESULTS/" # On server
nbcorestouse <- 10

#####

library(randomForest)
library(parallel)


# On PC 
#repOut <- "RESULTS/100simulatedDataSet/"      
#nbcorestouse <- detectCores()-1


# Calls to functions
source("scripts/randomForest/0-runRandomForestfct.r")
source("scripts/randomForest/1-randomForest_init.r")
source("scripts/randomForest/2-randomForest_regression.r")
source("scripts/randomForest/3-randomForest_result.r")


##########################################################################
#### Run          ########################################################
##########################################################################

# 100 simulated datasets             
namefilesExp <- list.files(path="data/100simulatedDataSet/artificialDataSet_expression", full.names = T, recursive = T, pattern = "*expression.tsv")
namefileSnp <- "data/100simulatedDataSet/artificialDataSet_haplotype.tsv"

# # Already done (from exp file names) : 
doneFiles <- list.files(repOut, recursive = T, pattern="rf_edgeListAll.tsv")

# Still to do: todoExp
if(length(doneFiles)>0){
  doneFiles <- substr(doneFiles, 1, regexpr("/", doneFiles)-1)
  doneFiles <- paste0("data/100simulatedDataSet/artificialDataSet_expression/", substr(doneFiles,1,2), "_", substr(doneFiles,3,4), "/", doneFiles, ".tsv")
  todoExp <- namefilesExp[-match(doneFiles, namefilesExp)]
} else {
  todoExp <- namefilesExp
}

# Here paralelization is done accross expression files, and not accross the randomForest
mclapply(todoExp, function(x){
  runrandomForest(x, namefileSnp, GenesAsRows = TRUE, dataScale = 1, 
                  nbTree = 1000, nbCores = 1, outDir = repOut, outFileName = NULL)
}, mc.cores = detectCores()-1)


