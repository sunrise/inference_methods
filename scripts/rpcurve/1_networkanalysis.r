source("scripts/rpcurve/network_analysis_function.r")

# Un peu long... (pas fait en parallèle pour pouvoir gérer les erreurs / fichiers manquants)
trueNetwork <- read.delim("data/100simulatedDataSet/artificialDataSet_network.csv", sep="\t", stringsAsFactors = F, header = F, col.names = c("Gj", "Gi"))
listdatasets <- list.dirs("RESULTS/100simulatedDataSet/", recursive = F, full.names = F)

# results of the methods
for(method in c("lasso", "PEMRF","randomForest", "OLS", "bayesianNetwork", "findr")){
  print(method)
  for(dataset in listdatasets){
    fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
    dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
    if(file.exists(fichier)){
      infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
      currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
      currentRPcurve <- currentRPcurve[,c("recall","precision")]
      rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
      if(dir.exists(dirout)==FALSE){
        dir.create(dirout)
      }
      write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
    } else {
      print(paste0("missing: ",fichier))
    }
  }
}

# results of the metaanalysis
for(method in c("metaAnalyse")){
  print(method)
  for(dataset in listdatasets){
    fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_metaanalysis/", method, "_edgeList.tsv")
    dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
    if(file.exists(fichier)){
      infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
      infNetwork <- infNetwork[,c("Gj", "Gi", "metaAnalyseScore")]
      colnames(infNetwork) <- c("Gj", "Gi", "score")
      currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
      rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
      if(dir.exists(dirout)==FALSE){
        dir.create(dirout)
      }
      write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
    } else {
      print(paste0("missing: ",fichier))
    }
  }
}

### Other methods tested
# 
# # results of the methods
# for(method in c("spaceMB")){
#   print(method)
#   for(dataset in listdatasets){
#     fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
#     dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
#     if(file.exists(fichier)){
#       infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
#       currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
#       currentRPcurve <- currentRPcurve[,c("recall","precision")]
#       rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
#       if(dir.exists(dirout)==FALSE){
#         dir.create(dirout)
#       }
#       write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
#     } else {
#       print(paste0("missing: ",fichier))
#     }
#   }
# }
# 
# # results of the methods
# for(method in c("genenetstatic", "genenetdynamic")){
#   print(method)
#   for(dataset in listdatasets){
#     fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
#     dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
#     if(file.exists(fichier)){
#       infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
#       currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
#       currentRPcurve <- currentRPcurve[,c("recall","precision")]
#       rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
#       if(dir.exists(dirout)==FALSE){
#         dir.create(dirout)
#       }
#       write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
#     } else {
#       print(paste0("missing: ",fichier))
#     }
#   }
# }
# 
# 
# # results of the methods
# for(method in c("geneBayesNet")){
#   print(method)
#   for(dataset in listdatasets){
#     fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
#     dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
#     if(file.exists(fichier)){
#       infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
#       currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
#       currentRPcurve <- currentRPcurve[,c("recall","precision")]
#       rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
#       if(dir.exists(dirout)==FALSE){
#         dir.create(dirout)
#       }
#       write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
#     } else {
#       print(paste0("missing: ",fichier))
#     }
#   }
# }
# 
# # results of the methods
# for(method in c("genenetstaticlambda")){
#   print(method)
#   for(dataset in listdatasets){
#     fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
#     dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
#     if(file.exists(fichier)){
#       infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
#       currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
#       currentRPcurve <- currentRPcurve[,c("recall","precision")]
#       rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
#       if(dir.exists(dirout)==FALSE){
#         dir.create(dirout)
#       }
#       write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
#     } else {
#       print(paste0("missing: ",fichier))
#     }
#   }
# }
# # 
# # 
# # 
# # results of the methods
# for(method in c("findr", "findr2")){
#   print(method)
#   for(dataset in listdatasets){
#     fichier <- paste0("RESULTS/100simulatedDataSet/", dataset, "/in_metaanalysis/", method, "_edgeList.tsv")
#     dirout <- paste0("RESULTS/100simulatedDataSet/", dataset, "/out_RPcurve")
#     if(file.exists(fichier)){
#       infNetwork <- read.delim(fichier, sep="\t", stringsAsFactors = F)
#       currentRPcurve <- networkAnalysis(trueNetwork = trueNetwork, infNetwork = infNetwork)
#       currentRPcurve <- currentRPcurve[,c("recall","precision")]
#       rownames(currentRPcurve) <- c(1:nrow(currentRPcurve))
#       if(dir.exists(dirout)==FALSE){
#         dir.create(dirout)
#       }
#       write.csv(currentRPcurve, file = paste0(dirout, "/", method, ".csv"), row.names = F)
#     } else {
#       print(paste0("missing: ",fichier))
#     }
#   }
# }