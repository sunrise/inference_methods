###########################################
# rp curve (+ figure 3)
###########################################""

rm(list=ls())
library(data.table)
library(ggplot2)

listDir <- list.dirs("RESULTS/100simulatedDataSet", recursive = F, full.names = T)
listDataset <- list.dirs("RESULTS/100simulatedDataSet", recursive = F, full.names = F)

# RPcurve
#################################################################################

function_rp_method <- function(method, datasetPaths=listDir, datasetNames=listDataset){
  list_method <- paste0(datasetPaths, "/out_RPcurve/", method, ".csv")
  
  rp_method <- lapply(list_method[file.exists(list_method)], function(x) fread(x))
  rp_method <- lapply(rp_method, function(x) {
    x <- data.table(x)
    x <- x[,.(recall, precision)] 
    x[,num:=rownames(x)]
    return(x)
  })
  names(rp_method) <- datasetNames[file.exists(list_method)]
  rp_method <- rbindlist(rp_method, idcol = "file")
  rp_method <- rp_method[,.(medianprec = median(precision, na.rm = TRUE),
                            medianrecall = median(recall, na.rm = TRUE),
                            upprec = quantile(x = precision, probs = 0.75, na.rm = TRUE),
                            downprec = quantile(x = precision, probs = 0.25, na.rm = TRUE),
                            uprecall = quantile(x = recall, probs = 0.75, na.rm = TRUE),
                            downrecall = quantile(x = recall, probs = 0.25, na.rm = TRUE)), 
                         by=.(num)]
  rp_method[,method := method]
  return(rp_method)
}

rp_randomforest <- function_rp_method("randomForest")
rp_lasso <- function_rp_method("lasso")
rp_OLS <- function_rp_method("OLS")
rp_pemrf <- function_rp_method("PEMRF")
rp_meta <- function_rp_method("metaAnalyse")
rp_findr <- function_rp_method("findr")
rp_GBN <- function_rp_method("geneBayesNet")

# Export to use in shiny app on methods contribution
# fwrite(rp_randomforest, file="../contribution_methods/data/rp_randomforest.csv")
# fwrite(rp_lasso, file="../contribution_methods/data/rp_lasso.csv")
# fwrite(rp_OLS, file="../contribution_methods/data/rp_OLS.csv")
# fwrite(rp_pemrf, file="../contribution_methods/data/rp_pemrf.csv")
# fwrite(rp_GBN, file="../contribution_methods/data/rp_GBN.csv")
# fwrite(rp_meta, file="../contribution_methods/data/rp_meta_complet.csv")
# fwrite(rp_findr, file="../contribution_methods/data/rp_findr.csv")

rpcomplet <- merge(rp_randomforest, rp_lasso, all = T)
rpcomplet <- merge(rpcomplet, rp_OLS, all = T)
rpcomplet <- merge(rpcomplet, rp_pemrf, all = T)
rpcomplet <- merge(rpcomplet, rp_GBN, all = T)
rpcomplet <- merge(rpcomplet, rp_meta, all = T)
rpcomplet <- merge(rpcomplet, rp_findr, all = T)

rpcomplet[,num:=as.numeric(as.character(num))]
setnames(rpcomplet, "method", "Methods")
rpcomplet[Methods=="metaAnalyse", Methods:="meta-analysis"]
rpcomplet[Methods=="PEMRF", Methods:="PE-MRF"]
rpcomplet[Methods=="geneBayesNet", Methods:="bayesianNetwork"]

fwrite(rpcomplet, file="RESULTS/100simulatedDataSet/rp_allmethods.csv")
# fwrite(rpcomplet, file="../contribution_methods/data/rp_allmethods.csv")

rpcomplet_ribbonup <- copy(rpcomplet)
rpcomplet_ribbondown <- copy(rpcomplet)
rpcomplet_ribbonup[,recall:=round(uprecall,3)]
rpcomplet_ribbondown[,recall:=round(downrecall,3)]
rpcomplet_ribbonup <- rpcomplet_ribbonup[,.(maxupprec=max(upprec)), by=.(Methods, recall)]
rpcomplet_ribbondown <- rpcomplet_ribbondown[,.(mindownprec=min(downprec)), by=.(Methods, recall)]
rpcomplet_ribbon <- merge(rpcomplet_ribbonup, rpcomplet_ribbondown, by=c("Methods", "recall"), all=T)

tp <- ggplot(rpcomplet) + 
  geom_line(aes(x=medianrecall, y=medianprec, group=Methods, colour=Methods))  + 
  geom_point(data=rpcomplet[Methods=="meta-analysis" & num %in% c(50, 100, 200)], aes(x=medianrecall, y=medianprec)) +
  geom_text(data=rpcomplet[Methods=="meta-analysis" & num %in% c(50, 100, 200)], aes(x=medianrecall+0.02, y=medianprec, label=num), color="black") +
  geom_point(data=rpcomplet[Methods=="meta-analysis" & num %in% c(64)], aes(x=medianrecall, y=medianprec), color="red") +
  geom_text(data=rpcomplet[Methods=="meta-analysis" & num %in% c(64)], aes(x=medianrecall+0.02, y=medianprec, label=num), color="red", fontface="bold")

 tp <- tp +  geom_ribbon(data=rpcomplet_ribbon,
                         aes(x=recall, 
                            ymin=mindownprec, 
                            ymax=maxupprec, fill=Methods),
                      color = "#FEFEFE00", alpha = 0.1, na.rm=T) +
  labs(y = "Precision", x="Recall") + # renomme l'axe des y
  theme_bw() + # fond blanc / theme du graphique
  xlim(0, 0.5) +
  ylim(0,1) + 
  theme(panel.border = element_blank()) +
  annotate("text",
               x=0.5, y=0.9, 
               label ="Curves represent the median\nover the 100 datasets.\nAreas cover space between\nthe 0.25 and 0.75 quantiles.",  
               color = "black", 
               size=3, hjust=1)
tp

pdf("RESULTS/graphs/rpcurve.pdf", width=8, height=4)
print(tp)
dev.off()


# ### Figure 3 (old) : boxplot of precisions by selected nb of edges
# 
# rm(list=ls())
# library(data.table)
# library(ggplot2)
# # Ouverture ds rpcurves
# 
# datasetPaths <- list.dirs("RESULTS/100simulatedDataSet", recursive = F, full.names = T)
# datasetNames <- list.dirs("RESULTS/100simulatedDataSet", recursive = F, full.names = F)
# 
# method <- "metaAnalyse"
# list_method <- paste0(datasetPaths, "/out_RPcurve/", method, ".csv")
# rp_points <- lapply(list_method[file.exists(list_method)], function(x) read.csv(x))
# rp_points <- lapply(rp_points, function(x) {
#  x <- data.table(x)
#  x <- x[,.(recall, precision)]
#  x[,num:=rownames(x)]
#  x <- x[num %in% c(50, 75, 100, 200)]
#  return(x)
# })
# names(rp_points) <- datasetNames[file.exists(list_method)]
# rp_points <- rbindlist(rp_points, idcol = "file")
# # get ggplot2 default color
# gg_color <- function(n) {
#  hues = seq(15, 375, length = n + 1)
#  hcl(h = hues, l = 65, c = 100)[1:n]
# }
# col <- gg_color(6)
# rp_points[,num:=as.numeric(as.character(num))]
# boxplotfig3 <- ggplot(data=rp_points, aes(x=as.factor(num), y=100*precision)) +
#                geom_boxplot(colour="black", fill="#b2eac2") + ylim(0,100) +
#                xlab("Number of selected edges") + ylab("Precision (%)") +
#                geom_hline(yintercept = 75, linetype="dashed", color = "black")  +
#                theme_bw() + # fond blanc / theme du graphique
#                theme(panel.border = element_blank())
# boxplotfig3

