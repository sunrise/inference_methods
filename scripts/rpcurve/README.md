# Meta-analysis on network inference methods

Compile the results from the different methods and the meta-analysis and analyse the
infered network.
- lasso
- random forest
- PE-MRF
- OLS
- bayesian network
- meta-analysis


## Compute recall precision [1_networkanalysis.r](./scripts/rpcurve/1_networkanalysis.r)

The scripts will compute for each method the recall and precision.

This script calls [network_analysis_function.r](./scripts/rpcurve/network_analysis_function.r).

## Draw the plot [2_rpcurve.R](./scripts/rpcurve/2_rpcurve.r)

The script draw the RPcurve : recall precision curve.

Required libraries : data.table, ggplot2

