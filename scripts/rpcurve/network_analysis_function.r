#------------------------------------------------------------------------#

#' count.orderEdge.TP
#' 
#' count the number of True Positive (TP) edges found in the inferred network, 
#' for each additional edge
#'
#' @param orderEdgeTpFp sorted vector of edges, 1 of TP, 0 otherwise
#'
#' @return countOrderEdgeTP: number of TP for each additional edge
#' @export
count.orderEdge.TP <- function(orderEdgeTpFp)
{
  countOrderEdgeTP <- orderEdgeTpFp
  
  for (i in 2:length(countOrderEdgeTP))
  {
    countOrderEdgeTP[i]<- countOrderEdgeTP[i-1]+orderEdgeTpFp[i]
  }
  return(countOrderEdgeTP)
}

#------------------------------------------------------------------------#

#' count.orderEdge.FP
#' 
#' count the number of False Positive (FP) edges found in the inferred network
#' (found in the inferred network but absent from the real network), for each 
#' additional edge
#'
#' @param orderEdgeNbTP number of TP (True Positive) found along the sorted 
#' vector of edges
#'
#' @return number of FP for each additional edge
#' @export
count.orderEdge.FP <- function(orderEdgeNbTP){
  countOrderEdgeFP <- orderEdgeNbTP
  
  for (i in 1:length(countOrderEdgeFP))
  {
    countOrderEdgeFP[i] <- i - orderEdgeNbTP[i]
  }
  return(countOrderEdgeFP)
}

#------------------------------------------------------------------------#

#' networkAnalysis
#' 
#' For a network with sorted edges (decreasing by frequency of founds),
#' compute the recall and the presition of the network, for each additional 
#' edge
#'
#' @param trueNetwork real network: data.frame of edges with two columns
#' Gj and Gi giving the genes names, each row representing an edge between the 
#' genes Gj and Gi
#' @param infNetwork infered network:  data.frame of edges with three columns
#' Gj and Gi giving the genes and score giving the resulting score of the inference 
#' method, each row representing an edge between the genes Gj and Gi.
#' @param possibleID possible patterns given to genes names in the inferred network
#' ("G" for genes, "E" for expressions and "S" for SNP) corresponding to genes in 
#' the real network

#' @return a data.frame giving in two columns the recall and the precision,
#' each row corresponding to an additional edge found in the inferred network
#' @export
networkAnalysis <- function(trueNetwork,infNetwork, possibleID = c("E","M","G"))
{
  # nb of edges in the real network
  nbEdgeTrueNetwork <- nrow(trueNetwork)
  
  # replace genes names in the inferred network to be like the real network
  t <- paste("(", paste(possibleID, collapse = ")|("), ")", sep = "")
  infNetwork$Gj <- sub(pattern = t, replacement = "G", x = infNetwork$Gj, perl = TRUE)
  infNetwork$Gi <- sub(pattern = t, replacement = "G", x = infNetwork$Gi, perl = TRUE)
  
  rownames(infNetwork) <- paste(infNetwork$Gj, infNetwork$Gi, sep = "-")
  
  # remove the edges between identical genes
  infNetWorkNoAutoReg <- infNetwork[infNetwork$Gj != infNetwork$Gi,]
  
  # Sort the edges of inferred network
  orderInfNetwork <- infNetWorkNoAutoReg[order(infNetWorkNoAutoReg$score, decreasing = TRUE),]
  
  # Add tp column : 1 if edge exists in real network, 0 otherwise
  orderInfNetwork$tp <- 1*(with(orderInfNetwork, paste(Gj,Gi)) %in% with(trueNetwork, paste(Gj,Gi)))
  
  # Add the number of TP, FP and FN, cumulative for each additional edge
  orderInfNetwork$nbTP <- count.orderEdge.TP(orderEdgeTpFp = orderInfNetwork$tp)
  orderInfNetwork$nbFP <- count.orderEdge.FP(orderEdgeNbTP = orderInfNetwork$nbTP)
  orderInfNetwork$nbFN <- nbEdgeTrueNetwork - orderInfNetwork$nbTP
  
  
  # Compute the recall and the precision
  # recall = TP/(TP+FN)
  orderInfNetwork$recall <- orderInfNetwork$nbTP/(orderInfNetwork$nbTP+orderInfNetwork$nbFN)
  # precision = TP/(TP+FP)
  orderInfNetwork$precision <- orderInfNetwork$nbTP/(orderInfNetwork$nbTP+orderInfNetwork$nbFP)
  
  return(orderInfNetwork)
}
